package it.uniba.ia.util;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import it.uniba.ia.evaluation.ESWC2015Model;
import net.recommenders.rival.core.DataModel;
import net.recommenders.rival.split.splitter.SplitterRunner;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Created by asuglia on 5/21/15.
 */
public class ESWCDataManager {
    public static BiMap<String, String> readResources(String filePath, String entity) throws IOException {
        BiMap<String, String> resourceMap = HashBiMap.create();

        try (CSVParser parser = new CSVParser(new FileReader(filePath), CSVFormat.DEFAULT)) {
            parser.getRecords().stream().
                    filter(record -> record.get(1).equals(entity)).
                    forEach(record -> resourceMap.put(record.get(0), record.get(2)));
        }

        return resourceMap;

    }

    public static Multimap<String, String> readRatings(String filePath, char separator) throws IOException {
        Multimap<String, String> ratings = HashMultimap.create();
        CSVFormat format = CSVFormat.newFormat(separator);

        try (CSVParser parser = new CSVParser(new FileReader(filePath), format)) {
            parser.getRecords().stream().
                    forEach(record -> ratings.put(record.get(0), record.get(1)));

        }

        return ratings;

    }

    public static void removeMetadataFromDataset(String origFile,
                                                 String newFile,
                                                 Map<String, String> itemInfo) throws IOException {
        try (CSVParser parser = new CSVParser(new FileReader(origFile), CSVFormat.DEFAULT)) {
            try (CSVPrinter printer = new CSVPrinter(new FileWriter(newFile), CSVFormat.DEFAULT)) {
                parser.getRecords().stream().
                        filter(record -> itemInfo.get(record.get(1)) != null).
                        forEach((values) -> {
                            try {
                                printer.printRecord(values);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
            }
        }

    }

    public static void main(String[] args) throws IOException {
        String origFile = "/home/asuglia/dataset/eswc2015/book/training_likes_books.csv",
                newFile = "/home/asuglia/dataset/eswc2015/book/training_likes_books_new.csv",
                itemInfoFile = "/home/asuglia/dataset/eswc2015/book/items_books.csv";

        BiMap<String, String> itemInfo = ESWCDataManager.readResources(itemInfoFile, "book");

        ESWCDataManager.removeMetadataFromDataset(origFile, newFile, itemInfo);


    }

}
