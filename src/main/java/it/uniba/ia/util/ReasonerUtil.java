package it.uniba.ia.util;

import aterm.ATermAppl;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.mindswap.pellet.KnowledgeBase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by asuglia on 6/6/15.
 */
public class ReasonerUtil {
    public static Map<ATermAppl, Multimap<ATermAppl, ATermAppl>>
        getAllIndividualsProperties(PelletReasoner reasoner) {
        KnowledgeBase kb = reasoner.getKB();

        Map<ATermAppl, Multimap<ATermAppl, ATermAppl>> allIndividualProp = new HashMap<>();

        Set<ATermAppl> properties = kb.getProperties();

        for(ATermAppl i : kb.getIndividuals()) {
            Multimap<ATermAppl, ATermAppl> currIndPropMap = HashMultimap.create();

            for(ATermAppl prop : properties) {

                List<ATermAppl> currIndividualProp = kb.getPropertyValues(prop, i);

                if(!currIndividualProp.isEmpty()) {
                    currIndividualProp.forEach(currProp -> currIndPropMap.put(prop, currProp));
                }

                allIndividualProp.put(i, currIndPropMap);

            }
        }

        return allIndividualProp;
    }

    public static Map<ATermAppl, Multimap<ATermAppl, ATermAppl>>
    getAllIndividualsProperties(PelletReasoner reasoner, ATermAppl individualClass) {
        KnowledgeBase kb = reasoner.getKB();

        Map<ATermAppl, Multimap<ATermAppl, ATermAppl>> allIndividualProp = new HashMap<>();

        Set<ATermAppl> properties = kb.getProperties();

        for(ATermAppl i : kb.getInstances(individualClass)) {
            Multimap<ATermAppl, ATermAppl> currIndPropMap = HashMultimap.create();

            for(ATermAppl prop : properties) {

                List<ATermAppl> currIndividualProp = kb.getPropertyValues(prop, i);

                if(!currIndividualProp.isEmpty()) {
                    currIndividualProp.forEach(currProp -> currIndPropMap.put(prop, currProp));
                }

                allIndividualProp.put(i, currIndPropMap);

            }
        }

        return allIndividualProp;
    }
}
