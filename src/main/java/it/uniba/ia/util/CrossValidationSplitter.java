package it.uniba.ia.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;

import it.uniba.ia.evaluation.ESWC2015Model;
import net.recommenders.rival.core.DataModel;
import net.recommenders.rival.core.DataModelUtils;
import net.recommenders.rival.split.splitter.Splitter;

/**
 * Class that splits a dataset using a cross validation technique (every
 * interaction in the data only appears once in each test split).
 *
 * @author <a href="http://github.com/abellogin">Alejandro</a>
 */
public class CrossValidationSplitter<U, I> implements Splitter<U, I> {

    /**
     * The number of folds that the data will be split into.
     */
    private int nFolds;
    /**
     * The flag that indicates if the split should be done in a per user basis.
     */
    private boolean perUser;
    /**
     * An instance of a Random class.
     */
    private Random rnd;

    /**
     * Constructor
     *
     * @param nFolds  number of folds that the data will be split into
     * @param perUser flag to do the split in a per user basis
     * @param seed    value to initialize a Random class
     */
    public CrossValidationSplitter(int nFolds, boolean perUser, long seed) {
        this.nFolds = nFolds;
        this.perUser = perUser;

        rnd = new Random(seed);
    }

    /**
     * @inheritDoc
     */
    @Override
    public DataModel<U, I>[] split(DataModel<U, I> data) {
        @SuppressWarnings("unchecked")
        final DataModel<U, I>[] splits = new DataModel[2 * nFolds];
        for (int i = 0; i < nFolds; i++) {
            splits[2 * i] = new DataModel<U, I>(); // training
            splits[2 * i + 1] = new DataModel<U, I>(); // test
        }
        if (perUser) {
            int n = 0;
            for (U user : data.getUsers()) {
                List<I> items = new ArrayList<I>(data.getUserItemPreferences().get(user).keySet());
                Collections.shuffle(items, rnd);
                for (I item : items) {
                    Double pref = data.getUserItemPreferences().get(user).get(item);
                    Set<Long> time = null;
                    if (data.getUserItemTimestamps().containsKey(user) && data.getUserItemTimestamps().get(user).containsKey(item)) {
                        time = data.getUserItemTimestamps().get(user).get(item);
                    }
                    int curFold = n % nFolds;
                    for (int i = 0; i < nFolds; i++) {
                        DataModel<U, I> datamodel = splits[2 * i]; // training
                        if (i == curFold) {
                            datamodel = splits[2 * i + 1]; // test
                        }
                        if (pref != null) {
                            datamodel.addPreference(user, item, pref);
                        }
                        if (time != null) {
                            for (Long t : time) {
                                datamodel.addTimestamp(user, item, t);
                            }
                        }
                    }
                    n++;
                }
            }
        } else {
            List<U> users = new ArrayList<U>(data.getUsers());
            Collections.shuffle(users, rnd);
            int n = 0;
            for (U user : users) {
                List<I> items = new ArrayList<I>(data.getUserItemPreferences().get(user).keySet());
                Collections.shuffle(items, rnd);
                for (I item : items) {
                    Double pref = data.getUserItemPreferences().get(user).get(item);
                    Set<Long> time = null;
                    if (data.getUserItemTimestamps().containsKey(user) && data.getUserItemTimestamps().get(user).containsKey(item)) {
                        time = data.getUserItemTimestamps().get(user).get(item);
                    }
                    int curFold = n % nFolds;
                    for (int i = 0; i < nFolds; i++) {
                        DataModel<U, I> datamodel = splits[2 * i]; // training
                        if (i == curFold) {
                            datamodel = splits[2 * i + 1]; // test
                        }
                        if (pref != null) {
                            datamodel.addPreference(user, item, pref);
                        }
                        if (time != null) {
                            for (Long t : time) {
                                datamodel.addTimestamp(user, item, t);
                            }
                        }
                    }
                    n++;
                }
            }
        }
        return splits;
    }

    public static void main(String[] args) throws IOException {
        String originalDatasetFile = "/home/asuglia/dataset/eswc2015/book/training_likes_books.csv",
                outputFormat = "/home/asuglia/dataset/eswc2015/book/cv/%s_%d.csv";
        int folds = 10, seed = 12345;

        CrossValidationSplitter<String, String> splitter = new CrossValidationSplitter<>(folds, true, seed);

        ESWC2015Model data = new ESWC2015Model();
        data.loadDataset(new File(originalDatasetFile));

        DataModel<String, String>[] splits = splitter.split(data);

        for (int i = 0; i < folds; i++) {
            // training
            DataModelUtils.saveDataModel(splits[2 * i], String.format(outputFormat, "train", i + 1), true);
            // test
            DataModelUtils.saveDataModel(splits[2 * i + 1], String.format(outputFormat, "test", i + 1), true);
        }
    }
}
