package it.uniba.ia.util;

import com.google.common.collect.BiMap;
import it.uniba.ia.sparql.PropertiesManager;

import java.io.IOException;

/**
 * Created by asuglia on 5/15/15.
 */
public class ResourceRetrieval {

    /**
     * args[0] - items DBpedia mapping
     * args[1] - Apache Jena TDB index directory
     * args[2] - DBpedia SPARQL endpoint
     * args[3] - entity ID in mapping file (e.g., "book", "movie", ...)
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String resourceFile = "/home/asuglia/dataset/eswc2015/book/items_books.dat",
                resourcesDir = "/home/asuglia/dataset/eswc2015/book/resources/",
                dbpediaEndpoint = "http://dbpedia.org/sparql",
                entityID = "book";

        BiMap<String, String> data = ESWCDataManager.readResources(resourceFile, entityID);

        PropertiesManager manager = new PropertiesManager(resourcesDir);

        int con = 0;
        for (String item : data.inverse().keySet()) {
            if (manager.existsResource(item))
                con++;
        }

        System.out.println(con);
        /*String queryFormat = "SELECT ?pred ?obj WHERE { %s ?pred ?obj }";
        SPARQLReader reader = new SPARQLReader();


        manager.start(true);

        int i = 0;

        for (String key : data.keySet()) {
            String uri = data.get(key);

            List<QuerySolution> entityData = reader.getEntityDataFromEndpoint(
                    dbpediaEndpoint,
                    String.format(queryFormat, "<" + uri + ">"));

            manager.addSolutions(uri, entityData);

            manager.addModelData(reader.constructEntity(dbpediaEndpoint, uri));

            if (++i % 50 == 0) {
                i = 0;
                myWait(15);
            }
        }

        manager.commitChanges();
        manager.closeManager(); */

    }

    private static void myWait(int sec) {
        //wait

        long cm = System.currentTimeMillis();
        while ((System.currentTimeMillis() - cm) < sec * 1000) ;

    }

    private static String getDbpediaDataURI(String originalURI) {
        String newURI = originalURI + ".rdf";

        newURI = newURI.replace("resource", "data");


        return newURI;
    }

}
