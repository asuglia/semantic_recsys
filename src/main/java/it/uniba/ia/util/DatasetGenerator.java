package it.uniba.ia.util;

import com.google.common.collect.BiMap;
import com.google.common.collect.Multimap;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemoveFolds;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by asuglia on 6/12/15.
 */
public class DatasetGenerator {
    public static Instances getInstancesForClustering(Map<String, String> itemInfo) {
        // Declare two numeric attributes
        List<String> itemsID = new ArrayList<>(),
                itemsIRI = new ArrayList<>();


        for (String id : itemInfo.keySet()) {
            itemsID.add(id);
            String iri = itemInfo.get(id);
            itemsIRI.add(iri);
        }


        Attribute itemsIRIAttribute = new Attribute("items_iri", itemsIRI),
                itemsIDAttribute = new Attribute("items_id", itemsID);

        // Generic instance: ID_ITEM IRI_ITEM
        ArrayList<Attribute> allAttribute = new ArrayList<>();

        allAttribute.add(itemsIDAttribute);
        allAttribute.add(itemsIRIAttribute);

        Instances dataset = new Instances("item_dataset", allAttribute, 0);


        itemInfo.keySet().stream().forEach(id -> {
            String iri = itemInfo.get(id);

            Instance i = new DenseInstance(2);
            i.setDataset(dataset);
            i.setValue(0, id);
            i.setValue(1, iri);

            dataset.add(i);

        });
        return dataset;

    }

    public static Instances getInstancesFromRatings(Collection<String> items) {
        Attribute itemsAttr = new Attribute("items", new ArrayList<>(items));

        ArrayList<String> classValues = new ArrayList<>();
        classValues.add("1");
        classValues.add("0");
        Attribute classAttr = new Attribute("class", classValues);

        ArrayList<Attribute> attrInfo = new ArrayList<>();

        attrInfo.add(itemsAttr);
        attrInfo.add(classAttr);

        Instances dataset = new Instances("ratings", attrInfo, 0);

        dataset.setClassIndex(1);

        for (String item : items) {
            Instance i = new DenseInstance(2);
            i.setDataset(dataset);
            i.setValue(0, item);
            i.setValue(1, "1");
            dataset.add(i);
        }

        return dataset;

    }

    public static Instances getRatingsWeka(String datasetFile,
                                           String fieldSep,
                                           boolean noHeader,
                                           BiMap<String, String> itemInfo) throws IOException {
        CSVLoader loader = new CSVLoader();
        loader.setFile(new File(datasetFile));
        loader.setFieldSeparator(fieldSep);
        loader.setNoHeaderRowPresent(noHeader);

        Instances dataset = loader.getDataSet();

        Attribute oldUsers = dataset.attribute(0),
                users = null,
                newItems = new Attribute("items", itemInfo.keySet().stream().collect(Collectors.toList()));

        dataset.deleteAttributeAt(0);
        dataset.deleteAttributeAt(0);

        ArrayList<String> userIds = new ArrayList<>();
        Enumeration<Object> userIt = oldUsers.enumerateValues();

        while (userIt.hasMoreElements()) {
            String id = (String) userIt.nextElement();
            userIds.add(id);
        }

        userIds.add("u0");

        users = new Attribute("users", userIds);

        dataset.insertAttributeAt(users, dataset.numAttributes());
        dataset.insertAttributeAt(newItems, dataset.numAttributes());
        dataset.setClassIndex(0);

        return dataset;

    }

    public static Instances getDatasetForUser(String user, Multimap<String, String> ratings) {
        Attribute itemsAttr = new Attribute("items", new ArrayList<>(new HashSet<>(ratings.values())));

        ArrayList<String> classValues = new ArrayList<>();
        classValues.add("0");
        classValues.add("1");
        Attribute classAttr = new Attribute("likes", classValues);

        ArrayList<Attribute> attrInfo = new ArrayList<>();

        attrInfo.add(itemsAttr);
        attrInfo.add(classAttr);

        Instances dataset = new Instances("ratings", attrInfo, 0);

        Collection<String> userItems = ratings.get(user);

        for (String currUser : ratings.keySet()) {
            Collection<String> items = ratings.get(user);

            // if the
            if (currUser.equals(user)) {
                for (String item : items) {

                    Instance itemInst = new DenseInstance(2);
                    itemInst.setDataset(dataset);
                    itemInst.setValue(0, item);
                    itemInst.setValue(1, "1");
                }
            } else {

                for (String item : items) {
                    // if it's not an already added item
                    if (!userItems.contains(item)) {

                        Instance itemInst = new DenseInstance(2);
                        itemInst.setDataset(dataset);
                        itemInst.setValue(0, item);
                        itemInst.setValue(1, "0");
                    }
                }
            }

        }

        dataset.setClassIndex(1);

        return dataset;
    }

    public static void createCVDatasets(String originalDatasetFile, String outputFormat, int folds, int seed) throws Exception {
        CSVLoader loader = new CSVLoader();
        loader.setSource(new File(originalDatasetFile));
        loader.setNoHeaderRowPresent(false);
        Instances originalDataset = loader.getDataSet();
        Logger logger = Logger.getLogger(DatasetGenerator.class.getName());

        for (int i = 1; i <= folds; i++) {

            RemoveFolds removeTrain = new RemoveFolds();

            removeTrain.setSeed(seed);
            removeTrain.setNumFolds(folds);
            removeTrain.setFold(i);
            removeTrain.setInvertSelection(true);
            removeTrain.setInputFormat(originalDataset);

            Instances trainOutput = Filter.useFilter(originalDataset, removeTrain);

            CSVSaver saver = new CSVSaver();
            String outputFile = String.format(outputFormat, "train", i);
            saver.setNoHeaderRow(true);
            saver.setFieldSeparator(",");
            saver.setFile(new File(outputFile));
            saver.setInstances(trainOutput);

            logger.info("Train " + outputFile + " - " + trainOutput.numInstances());
            saver.writeBatch();

            RemoveFolds removeTest = new RemoveFolds();

            removeTest.setSeed(seed);
            removeTest.setNumFolds(folds);
            removeTest.setFold(i);
            removeTest.setInputFormat(originalDataset);
            Instances testOutput = Filter.useFilter(originalDataset, removeTest);

            saver = new CSVSaver();
            outputFile = String.format(outputFormat, "test", i);
            saver.setNoHeaderRow(true);
            saver.setFieldSeparator(",");
            saver.setFile(new File(outputFile));
            saver.setInstances(testOutput);
            saver.writeBatch();

            logger.info("Test " + outputFile + " - " + testOutput.numInstances());

        }
    }

    public static void main(String[] args) throws Exception {
        String originalDatasetFile = "/home/asuglia/dataset/eswc2015/book/training_likes_books.csv",
                outputFormat = "/home/asuglia/dataset/eswc2015/book/cv/%s_%d.csv";
        int folds = 10, seed = 12345;

        createCVDatasets(originalDatasetFile, outputFormat, folds, seed);

    }


}
