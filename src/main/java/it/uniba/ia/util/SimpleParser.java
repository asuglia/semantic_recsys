package it.uniba.ia.util;

import net.recommenders.rival.core.DataModel;
import net.recommenders.rival.core.Parser;

import java.io.*;
import java.util.zip.GZIPInputStream;

public class SimpleParser {

    /**
     * The column index for the user id in the file.
     */
    public static final int USER_TOK = 0;
    /**
     * The column index for the item id in the file.
     */
    public static final int ITEM_TOK = 1;
    /**
     * The column index for the rating in the file.
     */
    public static final int RATING_TOK = 2;


    /**
     * Parse data file.
     *
     * @param f The file to be parsed.
     * @return A dataset created from the file.
     * @throws IOException if the file cannot be read.
     */
    public DataModel<String, String> parseData(File f) throws IOException {
        return parseData(f, "\t");
    }

    /**
     * Parse a data file with a specific separator between fields.
     *
     * @param f     The file to be parsed.
     * @param token The separator to be used.
     * @return A dataset created from the file.
     * @throws IOException if the file cannot be read.
     */
    public DataModel<String, String> parseData(File f, String token) throws IOException {
        DataModel<String, String> dataset = new DataModel<String, String>();

        BufferedReader br = new BufferedReader(new FileReader(f));
        String line = br.readLine();
        if (!line.matches(".*[a-zA-Z].*")) {
            parseLine(line, dataset, token);
        }
        while ((line = br.readLine()) != null) {
            parseLine(line, dataset, token);
        }
        br.close();

        return dataset;
    }


    /**
     * Parse line from data file.
     *
     * @param line    The line to be parsed.
     * @param dataset The dataset to add data from line to.
     * @param token   the token to split on
     */
    private void parseLine(String line, DataModel<String, String> dataset, String token) {
        String[] toks = line.split(token);
        // user
        String userId = toks[USER_TOK];
        // item
        String itemId = toks[ITEM_TOK];
        // preference
        double preference = Double.parseDouble(toks[RATING_TOK]);

        //////
        // update information
        //////
        dataset.addPreference(userId, itemId, preference);
    }
}
