package it.uniba.ia.util;

import it.uniba.ia.sparql.PropertiesManager;

import java.io.IOException;
import java.util.Map;

/**
 * Created by asuglia on 5/21/15.
 */
public class ResourceReader {
    public static void main(String[] args) throws IOException {
        String resourceFile = "/home/asuglia/dataset/eswc2015/book/items_books.dat",
                resourcesDir = "/home/asuglia/dataset/eswc2015/book/resources/",
                entityID = "book";
        PropertiesManager manager = new PropertiesManager(resourcesDir);
        Map<String, String> data = ESWCDataManager.readResources(resourceFile, entityID);
        manager.start(false);

        int co = 0;

        for (String key : data.keySet()) {
            if (manager.existsResource(data.get(key))) {
                co++;
            }
        }

        System.out.println(co == data.keySet().size());
    }

}
