package it.uniba.ia.util;


import it.uniba.ia.evaluation.ESWC2015Model;
import net.recommenders.rival.core.DataModel;
import net.recommenders.rival.core.DataModelUtils;
import net.recommenders.rival.split.splitter.Splitter;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Class that splits a dataset randomly.
 *
 * @author <a href="http://github.com/abellogin">Alejandro</a>
 */
public class RandomSplitter implements Splitter<String, String> {

    /**
     * The percentage of training to be used by the splitter.
     */
    private float percentageTraining;
    /**
     * The flag that indicates if the split should be done in a per user basis.
     */
    private boolean perUser;
    /**
     * The flag that indicates if the split should consider all the items
     * independently.
     */
    private boolean doSplitPerItems;
    /**
     * An instance of a Random class.
     */
    private Random rnd;

    /**
     * Constructor
     *
     * @param percentageTraining percentage of training data to be split
     * @param perUser            flag to do the split in a per user basis
     * @param seed               value to initialize a Random class
     * @param doSplitPerItems    if true, every interaction between a user and a
     *                           specific item is considered as one, and hence all of them will be either
     *                           on the training or on the test split
     */
    public RandomSplitter(float percentageTraining, boolean perUser, long seed, boolean doSplitPerItems) {
        this.percentageTraining = percentageTraining;
        this.perUser = perUser;
        this.doSplitPerItems = doSplitPerItems;

        rnd = new Random(seed);
    }

    /**
     * @inheritDoc
     */
    @Override
    public DataModel<String, String>[] split(DataModel<String, String> data) {
        @SuppressWarnings("unchecked")
        final DataModel<String, String>[] splits = new DataModel[2];
        splits[0] = new DataModel<String, String>(); // training
        splits[1] = new DataModel<String, String>(); // test
        if (perUser) {
            for (String user : data.getUsers()) {
                if (doSplitPerItems) {
                    List<String> items = new ArrayList<String>(data.getUserItemPreferences().get(user).keySet());
                    Collections.shuffle(items, rnd);
                    int splitPoint = Math.round(percentageTraining * items.size());
                    for (int i = 0; i < items.size(); i++) {
                        String item = items.get(i);
                        Double pref = data.getUserItemPreferences().get(user).get(item);
                        Set<Long> time = null;
                        if (data.getUserItemTimestamps().containsKey(user) && data.getUserItemTimestamps().get(user).containsKey(item)) {
                            time = data.getUserItemTimestamps().get(user).get(item);
                        }
                        DataModel<String, String> datamodel = splits[0]; // training
                        if (i > splitPoint) {
                            datamodel = splits[1]; // test
                        }
                        if (pref != null) {
                            datamodel.addPreference(user, item, pref);
                        }
                        if (time != null) {
                            for (Long t : time) {
                                datamodel.addTimestamp(user, item, t);
                            }
                        }
                    }
                } else {
                    if (!data.getUserItemTimestamps().containsKey(user)) {
                        continue;
                    }
                    List<String> itemsTime = new ArrayList<String>();
                    for (Entry<String, Set<Long>> e : data.getUserItemTimestamps().get(user).entrySet()) {
                        String i = e.getKey();
                        for (Long t : e.getValue()) {
                            itemsTime.add(i + "_" + t);
                        }
                    }
                    Collections.shuffle(itemsTime, rnd);
                    int splitPoint = Math.round(percentageTraining * itemsTime.size());
                    for (int i = 0; i < itemsTime.size(); i++) {
                        String it = itemsTime.get(i);
                        //Long item = Long.parseLong(it.split("_")[0]);
                        String item = it.split("_")[0];
                        Long time = Long.parseLong(it.split("_")[1]);
                        Double pref = data.getUserItemPreferences().get(user).get(item);
                        DataModel<String, String> datamodel = splits[0]; // training
                        if (i > splitPoint) {
                            datamodel = splits[1]; // test
                        }
                        if (pref != null) {
                            datamodel.addPreference(user, item, pref);
                        }
                        if (time != null) {
                            datamodel.addTimestamp(user, item, time);
                        }
                    }
                }
            }
        } else {
            for (String user : data.getUsers()) {
                for (String item : data.getUserItemPreferences().get(user).keySet()) {
                    Double pref = data.getUserItemPreferences().get(user).get(item);
                    Set<Long> time = null;
                    if (data.getUserItemTimestamps().containsKey(user) && data.getUserItemTimestamps().get(user).containsKey(item)) {
                        time = data.getUserItemTimestamps().get(user).get(item);
                    }
                    if (doSplitPerItems) {
                        DataModel<String, String> datamodel = splits[0]; // training
                        if (rnd.nextDouble() > percentageTraining) {
                            datamodel = splits[1]; // test
                        }
                        if (pref != null) {
                            datamodel.addPreference(user, item, pref);
                        }
                        if (time != null) {
                            for (Long t : time) {
                                datamodel.addTimestamp(user, item, t);
                            }
                        }
                    } else {
                        if (time != null) {
                            for (Long t : time) {
                                DataModel<String, String> datamodel = splits[0]; // training
                                if (rnd.nextDouble() > percentageTraining) {
                                    datamodel = splits[1]; // test
                                }
                                if (pref != null) {
                                    datamodel.addPreference(user, item, pref);
                                }
                                datamodel.addTimestamp(user, item, t);
                            }
                        } else {
                            DataModel<String, String> datamodel = splits[0]; // training
                            if (rnd.nextDouble() > percentageTraining) {
                                datamodel = splits[1]; // test
                            }
                            if (pref != null) {
                                datamodel.addPreference(user, item, pref);
                            }
                        }
                    }
                }
            }
        }
        return splits;
    }

    public static void main(String[] args) throws IOException {
        String originalDatasetFile = "/home/asuglia/dataset/eswc2015/book/training_likes_books.csv",
                testFile = "/home/asuglia/dataset/eswc2015/book/test_books.csv",
                trainFile = "/home/asuglia/dataset/eswc2015/book/train_books.csv";

        RandomSplitter splitter = new RandomSplitter(0.5f, false, 12345, false);
        ESWC2015Model data = new ESWC2015Model();
        data.loadDataset(new File(originalDatasetFile));

        DataModel<String, String>[] splits = splitter.split(data);

        //training
        DataModelUtils.saveDataModel(splits[0], trainFile, true);

        //test
        DataModelUtils.saveDataModel(splits[1], testFile, true);


    }

}