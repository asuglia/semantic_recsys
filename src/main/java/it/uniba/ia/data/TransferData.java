package it.uniba.ia.data;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe impiegata per poter trasferire i dati da utilizzare per le richieste
 * effettuate
 */
public class TransferData {
    private Map<String, Object> dataMap;

    /**
     * Costruisce una struttura che rappresenta un mapping tra le chiavi e i valori
     * specificati
     *
     * @param keys   identificativi dei valori
     * @param values valori associati agli identificativi
     */
    public TransferData(String[] keys, Object[] values) {
        if (dataMap != null) {
            dataMap.clear();
            dataMap.putAll(mapObjects(keys, values));
        } else {
            dataMap = mapObjects(keys, values);
        }

    }

    /**
     * Costruisce una struttura vuota
     */
    public TransferData() {
        this.dataMap = new HashMap<>();
    }

    private Map<String, Object> mapObjects(String[] keys, Object[] values) {
        if (keys.length != values.length)
            throw new RuntimeException("Keys and values don't match!");

        Map<String, Object> data = new HashMap<>();

        int i = 0;
        for (String key : keys) {
            data.put(key, values[i++]);
        }

        return data;
    }

    /**
     * Restituisce un dizionario costituito dalle sole coppie aventi come identificativi
     * quelli specificati nel vettore in input, estraendoli dalla struttura corrente.
     *
     * @param keys identificativi delle coppie richieste
     * @return dizionario di coppie
     */
    public TransferData extractByKeys(String[] keys) {
        TransferData extracted = new TransferData();
        for (String key : keys) {
            extracted.put(key, dataMap.get(key));
        }

        return extracted;
    }

    /**
     * Restituisce il valore associato all'identificativo specificato in input.
     *
     * @param id identificativo del valore richiesto
     * @return valore richiesto
     */
    public Object get(String id) {
        return dataMap.get(id);
    }

    /**
     * Inserisce una nuova coppia chiave-valore all'interno della struttura corrente
     *
     * @param id chiave
     * @param data valore
     */
    public void put(String id, Object data) {
        dataMap.put(id, data);
    }
}
