package it.uniba.ia.data;

import java.util.Set;
import java.util.TreeSet;

/**
 * Classe che viene utilizzata per rappresentare il punteggio di rilevanza associato ad uno specifico
 * identificatore
 */
public class ItemScore<K extends Comparable<K>, S extends Comparable<S>> implements Comparable<ItemScore<K, S>> {
    private K item;
    private S score;

    /**
     * Costuisce la coppia item-score sfruttando i parametri in input
     *
     * @param item  identificativo dell'oggetto
     * @param score punteggio associato all'oggetto
     */
    public ItemScore(K item, S score) {
        this.item = item;
        this.score = score;
    }

    /**
     * Restituisce l'identificativo della coppia
     *
     * @return identificativo
     */
    public K getItem() {
        return item;
    }

    /**
     * Restituisce il punteggio di rilevanza della coppia
     *
     * @return score
     */
    public S getScore() {
        return score;
    }

    /**
     * Confronta due istanze dapprima sulla base dello score ed in caso di
     * parità, sulla base del loro identificativo. Viene impiegata per ordinare
     * gli score in ordine descrescente.
     *
     * @param o istanza con la quale confrontare quella corrente
     * @return 0 se uguali, -1 se primo maggiore del secondo, 1 se primo minore del secondo
     */
    @Override
    public int compareTo(ItemScore<K, S> o) {
        if (o == null)
            return -1;

        if (this == o)
            return 0;

        // they are equals, return the object with max score
        if (this.item.equals(o.item))
            return 0;

        int tempComp = this.score.compareTo(o.score);

        return tempComp != 0 ? -1 * tempComp : -1 * this.item.compareTo(o.item);

    }

    @Override
    public String toString() {
        return "ItemScore{" +
                "item='" + item + '\'' +
                ", score=" + score +
                '}' + "\n";
    }

    public static void main(String[] args) {
        Set<ItemScore<String, Double>> sets = new TreeSet<>();

        sets.add(new ItemScore<>("u1", 0.8));
        sets.add(new ItemScore<>("u1", 0.8));
        sets.add(new ItemScore<>("u1", 0.8));
        sets.add(new ItemScore<>("u2", 0.4));

        System.out.println(sets);


    }
}
