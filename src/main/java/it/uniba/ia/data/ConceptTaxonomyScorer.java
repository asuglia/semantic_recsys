package it.uniba.ia.data;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.reasoner.Node;

import java.io.Serializable;
import java.util.*;

/**
 * Definisce la strategia di pesatura delle feature impiegate nel contesto
 * di riferimento della misura di similarità "Similarity in Context"
 */
public class ConceptTaxonomyScorer implements Serializable {
    private Map<OWLClass, Double> conceptScores;

    public ConceptTaxonomyScorer() {
        conceptScores = new HashMap<>();
    }

    /**
     * Genera i pesi associati ad ogni feature impiegando il reasoner fornito
     * in input.
     *
     * @param reasoner Reasoner used in order to compute features' weights
     */
    public void computeScore(PelletReasoner reasoner) {

        Map<OWLClass, Integer> heights = computeHeights(reasoner, 0);

        int depth = Collections.max(heights.values()).shortValue();

        for (OWLClass concept : heights.keySet()) {
            conceptScores.put(concept, (double) heights.get(concept) / depth);
        }

    }

    /**
     * Restistuisce il peso associato al concetto specificato in input
     *
     * @param concept concetto per il quale si intende ottenere il peso
     */
    public Double get(OWLClass concept) {
        return conceptScores.getOrDefault(concept, 0d);
    }


    private Map<OWLClass, Integer> computeHeights(PelletReasoner reasoner, int start) {
        Map<OWLClass, Integer> heights = new HashMap<>();
        Queue<Node<OWLClass>> nodesQueue = new LinkedList<>();
        nodesQueue.add(reasoner.getTopClassNode());
        Integer currLevel = start;

        while(!nodesQueue.isEmpty()) {
            Node<OWLClass> currNode = nodesQueue.poll();

            if (currNode.isTopNode()) {
                heights.put(currNode.getRepresentativeElement(), start);
            } else if (!currNode.isBottomNode()) {

                Node<OWLClass> fatherNode = null;
                Iterator<Node<OWLClass>> fatherNodeIt =
                        reasoner.getSuperClasses(currNode.getRepresentativeElement(), true).iterator();

                while (fatherNodeIt.hasNext() && fatherNode == null) {
                    Node<OWLClass> father = fatherNodeIt.next();

                    currLevel = heights.get(father.getRepresentativeElement());

                    if (currLevel != null)
                        fatherNode = father;
                }

                heights.put(currNode.getRepresentativeElement(), currLevel + 1);
            }

            nodesQueue.addAll(reasoner.getSubClasses(currNode.getRepresentativeElement(), true).getNodes());
        }



        return heights;
    }

}
