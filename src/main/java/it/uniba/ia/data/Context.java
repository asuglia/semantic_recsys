package it.uniba.ia.data;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.google.common.collect.BiMap;
import org.semanticweb.owlapi.model.*;

import java.util.HashSet;
import java.util.Set;

/**
 * Classe che definisce il contesto di riferimento dell'ontologia corrente
 */
public class Context {
    private Set<OWLClass> conceptList;

    /**
     * Costruisce il contesto corrente utilizzando il reasoner specificato
     *
     * @param reasoner reasoner utilizzato per la costruzione del contesto
     */
    public Context(PelletReasoner reasoner) {
        createConceptList(reasoner);
    }

    /**
     * Costruisce, mediante il reasoner specificato, il contesto corrente relativo agli individui
     * specificati in input
     *
     * @param reasoner reasoner
     * @param itemInfo informazioni sugli individui
     */
    public Context(PelletReasoner reasoner, BiMap<String, String> itemInfo) {
        createConceptList(reasoner, itemInfo);
    }

    private void createConceptList(PelletReasoner reasoner) {
        conceptList = reasoner.getSubClasses(reasoner.getTopClassNode().getRepresentativeElement(), false).getFlattened();

    }

    private void createConceptList(PelletReasoner reasoner, BiMap<String, String> itemInfo) {
        conceptList = new HashSet<>();
        OWLOntologyManager manager = reasoner.getRootOntology().getOWLOntologyManager();
        OWLDataFactory df = manager.getOWLDataFactory();
        OWLOntology ontology = reasoner.getRootOntology();

        for (String itemIRI : itemInfo.inverse().keySet()) {
            OWLNamedIndividual individual = df.getOWLNamedIndividual(IRI.create(itemIRI));

            ontology.getClassAssertionAxioms(individual).stream().forEach(c -> {
                conceptList.addAll(reasoner.getSuperClasses(c.getClassExpression(), false).getFlattened());
                conceptList.addAll(reasoner.getSubClasses(c.getClassExpression(), false).getFlattened());
            });

        }
    }

    /**
     * Restituisce la lista di feature che fanno parte del contesto corrente
     *
     * @return lista di concetti
     */
    public Set<OWLClass> getFeatureList() {
        return conceptList;
    }
}
