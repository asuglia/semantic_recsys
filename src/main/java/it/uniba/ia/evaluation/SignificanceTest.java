package it.uniba.ia.evaluation;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import it.uniba.ia.util.StatisticalSignificance;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe impiegata per generare i risultati del test statistico effettuato sui risultati
 * prodotti da tutti gli algoritmi presi in considerazione nello studio
 */
public class SignificanceTest {
    private static Logger currLogger = Logger.getLogger(SignificanceTest.class.getName());

    private static Map<String, Map<String, Double>> computeAllAlgoMetrics(String[] algoNames, String resultsFormat, Multimap<String, String> parameters) throws IOException {
        Map<String, Map<String, Double>> algoMetrics = new HashMap<>();

        for (String algo : algoNames) {
            for (String p : parameters.get(algo)) {
                String statFile = String.format(resultsFormat, algo, p);
                Map<String, Double> metrics = readPerUserMetric(statFile);
                algoMetrics.put(algo + "_" + p, metrics);
            }
        }
        return algoMetrics;
    }

    /**
     * Generare i risultati del test di significatività richiesto in accordo ad un file di properties
     * contenente tutti i parametri necessari all'esecuzione del programma.
     * <p>
     * I parametri necessari vengono specificati di seguito:
     * <p>
     * algorithms - identificativi degli algoritmi (e.g., algorithms=knn,pam)
     * knn - parametri da associare all'algoritmo knn (e.g., knn=35,70)
     * pam - vedi sopra
     * results_format - formato necessario per generare il path del file contenente le valutazioni per ogni utente
     * output_file - path del file che conterrà il csv con i valori del test statistico
     * stat_method - identificativo del metodo statistico
     *
     * @param args percorso del file di properties
     */
    public static void main(String[] args) {
        if (args.length == 1) {
            try {
                Properties prop = new Properties();
                prop.load(new FileReader(args[0]));

                String[] algoNames = prop.getProperty("algorithms").split(",");
                String resultsFormat = prop.getProperty("results_format"),
                        outputFile = prop.getProperty("output_file"),
                        statMethod = prop.getProperty("stat_method");

                Multimap<String, String> parameters = loadParameters(algoNames, prop);
                Map<String, Map<String, Double>> algoMetrics = computeAllAlgoMetrics(algoNames, resultsFormat, parameters);

                try (CSVPrinter printer = new CSVPrinter(new FileWriter(outputFile), CSVFormat.TDF)) {
                    printer.printRecord("algo1", "algo2", "p-value");
                    //for each algorithm
                    for (String algo : algoNames) {
                        for (String k : parameters.get(algo)) {
                            String algoID = algo + "_" + k;
                            Map<String, Double> baseline = algoMetrics.get(algoID);
                            for (String otherAlgo : algoNames) {
                                for (String otherK : parameters.get(otherAlgo)) {
                                    // significance test between different algorithm
                                    String otherID = otherAlgo + "_" + otherK;
                                    if (!algoID.equals(otherID)) {
                                        currLogger.info(String.format("Computing test for %s and %s", algoID, otherID));
                                        Map<String, Double> test = algoMetrics.get(otherID);
                                        StatisticalSignificance sign = new StatisticalSignificance(baseline, test);
                                        printer.printRecord(algoID, otherID, sign.getPValue(statMethod));
                                    }
                                }
                            }
                        }

                    }
                }


            } catch (Exception ex) {
                Logger.getLogger(SignificanceTest.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            System.err.println("Number of arguments not valid.");
            System.out.println();
            System.out.println("Usage: <properties_file>");
        }

    }

    private static Multimap<String, String> loadParameters(String[] algoNames, Properties prop) {
        Multimap<String, String> parameters = HashMultimap.create();

        for (String algo : algoNames) {
            String[] params = prop.getProperty(algo).split(",");
            for (String p : params)
                parameters.put(algo, p);
        }

        return parameters;

    }

    private static Map<String, Double> readPerUserMetric(String testFile) {
        Map<String, Double> perUserMetric = new HashMap<>();

        try (CSVParser parser = new CSVParser(new FileReader(testFile), CSVFormat.TDF)) {
            for (CSVRecord line : parser) {
                //skip header
                if (line.getRecordNumber() == 1)
                    continue;

                perUserMetric.put(line.get(0), Double.parseDouble(line.get(1)));
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return perUserMetric;
    }
}