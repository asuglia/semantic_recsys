package it.uniba.ia.evaluation;

import it.uniba.ia.util.SimpleParser;
import net.recommenders.rival.core.DataModel;
import net.recommenders.rival.evaluation.metric.ranking.Precision;
import net.recommenders.rival.evaluation.metric.ranking.Recall;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Classe che provvede a generare le liste di raccomandazioni prodotte da uno specifico algoritmo
 *
 */
public class ExperimentRunner {
    private static final Logger logger = Logger.getLogger(ExperimentRunner.class.getName());

    /**
     * Per la sua esecuzione impiega un file di Properties contenente alcuni dati necessari
     * per dare inizio all'esecuzione dell'algoritmo.
     * <p>
     * Il contenuto del file deve necessariamente contenere i seguenti parametri:
     * <p>
     * pred_format - formato impiegato per generare il nome del file contenente le raccomandazioni
     * test_format - formato impiegato per generare il nome del file contenente il test set
     * results_format - formato impiegato per generare il nome del file contenente i risultati
     * folds - numero di fold
     * cutoff - soglia alla quale calcolare la F1-measure
     * algo - identificativo dell'algoritmo (knn, pam)
     * param - parametri dell'algoritmo (e.g.,knn=val1,val2)
     *
     * @param args percorso del file di properties
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        if (args.length == 1) {
            Properties prop = new Properties();
            prop.load(new FileReader(args[0]));
            String predictionFileFormat = prop.getProperty("pred_format"),
                    testFileFormat = prop.getProperty("test_format"),
                    resultsFormat = prop.getProperty("results_format"),
                    algorithm = prop.getProperty("algo"),
                    paramStr = prop.getProperty("param");

            int folds = Integer.parseInt(prop.getProperty("folds"));
            int cutoff = Integer.parseInt(prop.getProperty("cutoff"));

            double cumulativePrecision = 0d, finalPrecision,
                    cumulativeRecall = 0d, finalRecall;

            Map<String, Double> cumulativePrecPerUser = new HashMap<>(),
                    cumulativeRecPerUser = new HashMap<>(),
                    fMeasurePerUser = new HashMap<>();
            int[] kLevels = null;

            if (paramStr != null) {
                String[] kStringValues = paramStr.split(",");

                kLevels = new int[kStringValues.length];

                for (int i = 0; i < kStringValues.length; i++) {
                    kLevels[i] = Integer.parseInt(kStringValues[i]);
                }
            }

            if (kLevels != null) {
                for (int k : kLevels) {
                    for (int i = 1; i <= folds; i++) {
                        String predictionFileName = String.format(predictionFileFormat, algorithm, k, i),
                                testFileName = String.format(testFileFormat, i);

                        SimpleParser parser = new SimpleParser();
                        DataModel<String, String> predictions = parser.parseData(new File(predictionFileName));

                        parser = new SimpleParser();
                        DataModel<String, String> testData = parser.parseData(new File(testFileName));

                        Precision<String, String> precision = new Precision<>(predictions, testData, 1d, new int[]{cutoff});
                        Recall<String, String> recall = new Recall<>(predictions, testData, 1d, new int[]{cutoff});
                        precision.compute();
                        recall.compute();

                        precision.getValuePerUser().keySet().stream().
                                forEach(u -> cumulativePrecPerUser.put(u,
                                        cumulativePrecPerUser.getOrDefault(u, 0d) + precision.getValueAt(u, cutoff)));

                        recall.getValuePerUser().keySet().stream().
                                forEach(u -> cumulativeRecPerUser.put(u,
                                        cumulativeRecPerUser.getOrDefault(u, 0d) + recall.getValueAt(u, cutoff)));

                        double currPrec = precision.getValueAt(cutoff), currRec = recall.getValueAt(cutoff);
                        cumulativePrecision += currPrec;
                        cumulativeRecall += currRec;
                        String partResults = String.format(resultsFormat, algorithm, k, i);
                        serializePerformances(partResults, "F1@10", f1Measure(currPrec, currRec));
                    }

                    for (String user : cumulativePrecPerUser.keySet()) {
                        double prec = cumulativePrecPerUser.get(user) / folds,
                                rec = cumulativeRecPerUser.get(user) / folds;

                        fMeasurePerUser.put(user, f1Measure(prec, rec));
                    }

                    serializePerUserMetric(fMeasurePerUser, resultsFormat, algorithm, String.valueOf(k));

                    finalPrecision = cumulativePrecision / folds;
                    finalRecall = cumulativeRecall / folds;
                    double finalF1 = f1Measure(finalPrecision, finalRecall);
                    logger.info("F1@" + cutoff + " = " + finalF1);
                    String complResults = String.format(resultsFormat, algorithm, k, "all");
                    serializePerformances(complResults, "F1@10", finalF1);
                }
            } else {
                for (int i = 1; i <= folds; i++) {
                    String predictionFileName = String.format(predictionFileFormat, algorithm, i),
                            testFileName = String.format(testFileFormat, i);

                    SimpleParser parser = new SimpleParser();
                    DataModel<String, String> predictions = parser.parseData(new File(predictionFileName));

                    parser = new SimpleParser();
                    DataModel<String, String> testData = parser.parseData(new File(testFileName));

                    Precision<String, String> precision = new Precision<>(predictions, testData, 1d, new int[]{cutoff});
                    Recall<String, String> recall = new Recall<>(predictions, testData, 1d, new int[]{cutoff});
                    precision.compute();
                    recall.compute();

                    precision.getValuePerUser().keySet().stream().
                            forEach(u -> cumulativePrecPerUser.put(u,
                                    cumulativePrecPerUser.getOrDefault(u, 0d) + precision.getValueAt(u, cutoff)));

                    recall.getValuePerUser().keySet().stream().
                            forEach(u -> cumulativeRecPerUser.put(u,
                                    cumulativeRecPerUser.getOrDefault(u, 0d) + recall.getValueAt(u, cutoff)));

                    double currPrec = precision.getValueAt(cutoff), currRec = recall.getValueAt(cutoff);
                    cumulativePrecision += currPrec;
                    cumulativeRecall += currRec;
                    String partResults = String.format(resultsFormat, algorithm, i);
                    serializePerformances(partResults, "F1@10", f1Measure(currPrec, currRec));
                }

                for (String user : cumulativePrecPerUser.keySet()) {
                    double prec = cumulativePrecPerUser.get(user) / folds,
                            rec = cumulativeRecPerUser.get(user) / folds;

                    fMeasurePerUser.put(user, f1Measure(prec, rec));
                }

                serializePerUserMetric(fMeasurePerUser, resultsFormat, algorithm, null);

                finalPrecision = cumulativePrecision / folds;
                finalRecall = cumulativeRecall / folds;
                double finalF1 = f1Measure(finalPrecision, finalRecall);
                logger.info("F1@" + cutoff + " = " + finalF1);
                String complResults = String.format(resultsFormat, algorithm, "all");
                serializePerformances(complResults, "F1@10", finalF1);
            }
        } else {
            logger.severe("Invalid arguments -> <properties_file_name>");
        }
    }

    private static double f1Measure(double p, double r) {
        return (2 * p * r) / (p + r);
    }

    private static void serializePerUserMetric(Map<String, Double> metricValues, String resultDirFormat, String algo, String param) throws IOException {
        String results = (param != null)
                ? String.format(resultDirFormat, algo, param, "users") : String.format(resultDirFormat, algo, "users");

        try (CSVPrinter printer = new CSVPrinter(new FileWriter(results), CSVFormat.TDF)) {
            printer.printRecord("user", "f1");

            for (String user : metricValues.keySet()) {
                printer.printRecord(user, metricValues.get(user));
            }
        }
    }

    private static void serializePerformances(String results,
                                              String metric,
                                              Double value)
            throws IOException {

        try (CSVPrinter printer = new CSVPrinter(new FileWriter(results), CSVFormat.TDF)) {
            printer.printRecord(metric);
            printer.printRecord(value);
        }

    }

}
