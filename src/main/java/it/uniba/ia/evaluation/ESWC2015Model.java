package it.uniba.ia.evaluation;

import net.recommenders.rival.core.DataModel;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

/**
 * Classe che rappresenta il modello di riferimento per il dataset impiegato
 * per la challenge ESWC2015
 */
public class ESWC2015Model extends DataModel<String, String> {
    /**
     * Carica il dataset a partire dal file specificato in input
     *
     * @param dataSource file contenente il dataset
     * @throws IOException errore nel caricamento del file
     */
    public void loadDataset(File dataSource) throws IOException {
        try (CSVParser parser = new CSVParser(new FileReader(dataSource), CSVFormat.DEFAULT)) {

            for (CSVRecord record : parser) {
                addPreference(record.get(0), record.get(1), 1d);
            }
        }
    }

}
