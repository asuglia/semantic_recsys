package it.uniba.ia.recsys.clust;

import it.uniba.ia.similarity.SimilarityMap;
import java.util.HashSet;
import java.util.List;

/**
 * Interfaccia che definisce un metodo per selezionare i medoidi di riferimento per
 * l'esecuzione dell'algoritmo di clustering (fase BUILD)
 * */
public interface KMedoidsInitialization<V extends Comparable<V>> {
    /**
     * Restituisce l'insieme dei medoidi iniziali
     *
     * @param items         insieme complessivo degli item
     * @param k             numero di cluster da generare
     * @param similarityMap matrice di similarità
     * @return insieme di k medoidi
     */
    HashSet<V> getInitialMedoids(List<V> items, int k, SimilarityMap<V, Double> similarityMap);
}
