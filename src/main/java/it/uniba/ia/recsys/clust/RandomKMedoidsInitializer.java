package it.uniba.ia.recsys.clust;

import it.uniba.ia.similarity.SimilarityMap;
import weka.core.Instances;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe che implementa la selezione casuale dei medoidi fra quelli presenti nel dataset
 */
public class RandomKMedoidsInitializer<V extends Comparable<V>> implements KMedoidsInitialization<V> {

    @Override
    public HashSet<V> getInitialMedoids(List<V> items, int k, SimilarityMap<V, Double> similarityMap) {

        List<V> shuffled = new ArrayList<>(items);

        Collections.shuffle(shuffled);

        return new HashSet<>(shuffled.stream().limit(k).collect(Collectors.toList()));
    }
}
