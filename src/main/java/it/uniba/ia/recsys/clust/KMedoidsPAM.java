package it.uniba.ia.recsys.clust;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.similarity.SimilarityMap;
import org.apache.commons.math3.stat.descriptive.summary.Sum;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Algoritmo PAM o k-medoids, proposto da Kaufman
 * e Rousseeuw in "Partitioning Around Medoids".
 * <p>
 * Reference:
 * <p>
 * Clustering my means of Medoids<br />
 * Kaufman, L. and Rousseeuw, P.J.<br />
 * in: Statistical Data Analysis Based on the L1-Norm and Related Methods
 * </p>
 */

public class KMedoidsPAM {
    private static final Logger logger = Logger.getLogger(KMedoidsPAM.class.getName());

    protected int k;

    protected int maxIter;

    protected KMedoidsInitialization<String> initializer;

    private SimilarityMap<String, Double> similarityMap;

    private Multimap<String, String> clusteredItems;

    /**
     * Construttore.
     *
     * @param similarityMap matrice di distanze
     * @param k           numero di cluster
     * @param maxiter     numero di iterate massime
     * @param initializer funzione per la selezione dei medoidi iniziali
     */
    public KMedoidsPAM(SimilarityMap<String, Double> similarityMap,
                       int k,
                       int maxiter,
                       KMedoidsInitialization<String> initializer) {
        this.similarityMap = similarityMap;
        this.k = k;
        this.maxIter = maxiter;
        this.initializer = initializer;
    }

    /**
     * Esegue l'algoritmo utilizzando l'insieme di item specificato in input
     * @param datasetItems insieme di item sul quale eseguire il clustering
     */
    public void run(Collection<String> datasetItems) {
        HashSet<String> medoids = initializer.getInitialMedoids(
                datasetItems.stream().collect(Collectors.toList()), k, similarityMap);

        logger.info("Medoids initialization completed.");
        Multimap<String, ItemScore<String, Double>> finalConfiguration = runPAMOptimization(datasetItems, medoids);
        logger.info("Final configuration generated using PAM.");
        generateClustering(finalConfiguration);
        logger.info("Items associated to clusters");

    }

    /**
     * Restituisce un dizionario le cui chiavi sono gli identificativi dei medoidi
     * e i valori associati sono delle liste degli item presenti nei cluster da questi rappresentati
     *
     * @return clustering generato
     */
    public Multimap<String, String> getClusteredItems() {
        return clusteredItems;
    }

    public void serializeGeneratedClustering(File clusterFile) throws IOException {
        try (PrintWriter writer = new PrintWriter(new FileWriter(clusterFile))) {
            for (String medoid : clusteredItems.keySet()) {
                StringBuilder builder = new StringBuilder();
                Collection<String> cluster = clusteredItems.get(medoid);
                int i = 1, sizeClust = cluster.size();
                for (String item : cluster) {
                    if (i != sizeClust)
                        builder.append(item).append(",");
                    else
                        builder.append(item);
                    ++i;
                }

                writer.printf("%s-%s\n", medoid, builder.toString());

            }

        }

    }

    private void generateClustering(Multimap<String, ItemScore<String, Double>> finalConfiguration) {
        clusteredItems = HashMultimap.create();

        for (String medoid : finalConfiguration.keySet()) {
            Collection<ItemScore<String, Double>> clusterItems = finalConfiguration.get(medoid);
            clusterItems.forEach(i ->
                    clusteredItems.put(medoid,
                            i.getItem()));
        }

    }

    private Multimap<String, ItemScore<String, Double>> associateToBestCluster(Collection<String> datasetItems,
                                                                               HashSet<String> medoids) {
        int totItems = datasetItems.size();
        Multimap<String, ItemScore<String, Double>> clusters = HashMultimap.create(k, totItems / k);

        for (String currInstance : datasetItems) {
            // if the current instance is not a medoid
            if (!medoids.contains(currInstance)) {
                LinkedHashMap<String, Double> scores = similarityMap.scores(currInstance, 1);
                // if is empty is not a book -> ignore it
                if (!scores.isEmpty()) {
                    Map.Entry<String, Double> bestMedoid = scores.entrySet().iterator().next();
                    clusters.put(bestMedoid.getKey(), new ItemScore<>(currInstance, bestMedoid.getValue()));
                }
            }
        }

        return clusters;
    }

    protected Multimap<String, ItemScore<String, Double>> runPAMOptimization(
            Collection<String> datasetItems,
            HashSet<String> medoids) {
        Multimap<String, ItemScore<String, Double>> bestConfiguration =
                associateToBestCluster(datasetItems, medoids);
        HashSet<String> oldMedoids = medoids, newMedoids = oldMedoids;
        Double pastCost = configurationCost(bestConfiguration);
        int numIter = 0;

        do {
            for (String medoid : medoids) {
                for (String instance : datasetItems) {
                    if (!medoids.contains(instance)) {
                        newMedoids = swapMedoid(medoid, instance, medoids);
                        Multimap<String, ItemScore<String, Double>> newClusterAssoc =
                                associateToBestCluster(datasetItems, newMedoids);

                        Double currCost = configurationCost(newClusterAssoc);

                        if (currCost < pastCost) {
                            bestConfiguration = newClusterAssoc;
                            oldMedoids = newMedoids;
                        }
                    }

                }

            }

            numIter++;
            logger.info(numIter + " iteration finished.");
        } while (!oldMedoids.equals(newMedoids) && numIter != maxIter);

        return bestConfiguration;
    }

    private Double configurationCost(Multimap<String, ItemScore<String, Double>> clusterAssoc) {
        Sum summer = new Sum();

        for (ItemScore<String, Double> itemScore : clusterAssoc.values()) {
            summer.increment(itemScore.getScore());
        }

        return summer.getResult();
    }

    private HashSet<String> swapMedoid(String medoid, String oldMedoid, HashSet<String> medoids) {
        HashSet<String> newMedoids = new HashSet<>(medoids);
        newMedoids.remove(oldMedoid);
        newMedoids.add(medoid);

        return newMedoids;
    }
}
