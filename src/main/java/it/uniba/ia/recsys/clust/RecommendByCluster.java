package it.uniba.ia.recsys.clust;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multimap;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.data.TransferData;
import it.uniba.ia.recsys.ItemRanker;
import it.uniba.ia.similarity.SimilarityEngine;
import it.uniba.ia.similarity.SimilarityMap;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Classe che utilizza l'algoritmo K-Medoids PAM per poter generare le raccomandazioni
 * per ogni utente
 */
public class RecommendByCluster implements ItemRanker {
    private static final Logger logger = Logger.getLogger(RecommendByCluster.class.getName());
    private final SimilarityEngine engine;
    private final int maxIter;
    private final int k;
    private Multimap<String, String> usersRatings;
    private HashMultiset<String> itemUserPopularity;
    private KMedoidsPAM pam;


    /**
     * Inizializza l'istanza corrente impiegando i parametri specificati nella
     * struttura in input.
     * <p>
     * Chiavi dei parametri:
     * K - numero di cluster
     * MAX_ITER - numero massimo di iterate per l'algoritmo di clustering
     * SIM_ENGINE - oggetto che gestisce la matrice di similarità
     *
     * @param data
     */
    public RecommendByCluster(TransferData data) {
        this.k = (int) data.get("K");
        this.maxIter = (int) data.get("MAX_ITER");
        this.engine = (SimilarityEngine) data.get("SIM_ENGINE");
    }

    /**
     * Calcola la popolarità di ogni item ed esegue l'algoritmo di clustering
     *
     * @param usersRatings espressione di gradimento degli utenti
     */
    public void build(Multimap<String, String> usersRatings) {
        this.usersRatings = usersRatings;
        this.itemUserPopularity = HashMultiset.create();

        // count how many time an item has been seen by a user
        for (String user : usersRatings.keySet()) {
            usersRatings.get(user).forEach(itemUserPopularity::add);
        }

        logger.info("Item popularity calculated.");

        this.pam = new KMedoidsPAM(engine.getSimilarityMap(), k, maxIter, new RandomKMedoidsInitializer<>());

        this.pam.run(new HashSet<>(usersRatings.values().stream().collect(Collectors.toSet())));

        logger.info("Finished PAM optimization");

    }

    /**
     * Genera le raccomandazioni per l'utente specificato secondo il
     * clustering generato e le sue preferenze presenti nel training set.
     *
     * @param user utente di riferimento
     * @return lista di item ordinati rispetto alla loro rilevanza
     * @throws Exception errore nella generazione della lista di raccomandazioni
     */
    @Override
    public Set<ItemScore<String, Double>> rank(String user) throws Exception {
        Set<ItemScore<String, Double>> itemsList = new TreeSet<>();
        SimilarityMap<String, Double> simMap = engine.getSimilarityMap();
        Collection<String> ratedItems = usersRatings.get(user);
        int totItems = usersRatings.values().stream().collect(Collectors.toSet()).size();
        Multimap<String, String> clusteredItems = pam.getClusteredItems();

        if (ratedItems != null) {
            Collection<String> cluster = null;
            for (String ratedItem : ratedItems) {
                for (String medoid : clusteredItems.keySet()) {
                    cluster = clusteredItems.get(medoid);
                    if (cluster.contains(ratedItem)) {
                        break;
                    }
                }

                if (cluster != null) {
                    cluster.stream().
                            filter(currItem -> !currItem.equals(ratedItem) && !ratedItems.contains(currItem)).
                            forEach(currItem -> {
                                double popScore = Math.log10(totItems / (itemUserPopularity.count(currItem) + 1)),
                                        simScore = simMap.get(ratedItem, currItem);

                                itemsList.add(new ItemScore<>(currItem, popScore * simScore));
                            });
                }
            }
        } else {
            itemUserPopularity.stream().
                    forEach(i -> itemsList.add(new ItemScore<>(i, Math.log10(totItems / (itemUserPopularity.count(i) + 1)))));
        }


        return itemsList;
    }

}
