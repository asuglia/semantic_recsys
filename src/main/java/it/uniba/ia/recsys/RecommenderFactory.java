package it.uniba.ia.recsys;

import it.uniba.ia.data.TransferData;
import it.uniba.ia.recsys.clust.RecommendByCluster;
import it.uniba.ia.recsys.lazy.NaiveKNNRecommender;
import it.uniba.ia.recsys.naive.RecommendByPopularity;

/**
 * Classe factory per gli algoritmi di recommendation implementati
 */
public class RecommenderFactory {
    public enum RecType {
        WEKA_KNN,
        NAIVE_KNN,
        PAM,
        POPULAR
    }

    /**
     * Restituisce istanza dei differenti algoritmi implementati utilizzando
     * i parametri passati in input
     */
    public static ItemRanker create(TransferData data) {
        RecType type = (RecType) data.get("RECTYPE");

        switch (type) {
            case NAIVE_KNN:
                return new NaiveKNNRecommender(data.extractByKeys(new String[]{
                        "NEIGHBOURS",
                        "SIM_ENGINE",
                        "ITEM_INFO",

                }));

            case PAM:
                return new RecommendByCluster(data.extractByKeys(new String[]{
                        "K",
                        "MAX_ITER",
                        "SIM_ENGINE",
                }));

            case POPULAR:
                return new RecommendByPopularity(data.extractByKeys(new String[]{
                        "TOP_N",
                }));

            default:
                throw new UnsupportedOperationException("Not available algorithm");

        }

    }

}
