package it.uniba.ia.recsys;

import com.google.common.collect.Multimap;
import it.uniba.ia.data.ItemScore;

import java.util.List;
import java.util.Set;

/**
 * Interfaccia che definisce un recommender system il cui compito è quello
 * della top-n recommendation
 */
public interface ItemRanker {
    /**
     * Restituisce la lista di raccomandazioni per l'utente indicato
     *
     * @param user identificativo utente
     * @return lista di raccomandazioni
     * @throws Exception errore nella generazione della lista
     */
    Set<ItemScore<String, Double>> rank(String user) throws Exception;

    /**
     * Inizializza recommender systems utilizzando le preferenze degli utenti
     * @param usersRatings rating utenti
     * @throws Exception errore nell'inizializzazione
     */
    void build(Multimap<String, String> usersRatings) throws Exception;
}
