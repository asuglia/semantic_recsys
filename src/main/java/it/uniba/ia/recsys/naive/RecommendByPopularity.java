package it.uniba.ia.recsys.naive;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultiset;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.data.TransferData;
import it.uniba.ia.recsys.ItemRanker;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Classe che genera raccomandazioni sulla base di uno score di popolarità
 */
public class RecommendByPopularity implements ItemRanker {
    private final int n;
    private TreeMultiset<String> ratingsForItem;

    public RecommendByPopularity(TransferData data) {
        this.n = (int) data.get("TOP_N");
        ratingsForItem = TreeMultiset.create();
    }

    private class PopularityComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            int tempComp = Integer.compare(ratingsForItem.count(o1), ratingsForItem.count(o2));

            return tempComp != 0 ? -1 * tempComp : o1.compareTo(o2);
        }
    }


    @Override
    public Set<ItemScore<String, Double>> rank(String user) throws Exception {
        TreeSet<String> popSet = new TreeSet<>(new PopularityComparator());

        ratingsForItem.stream().forEach(popSet::add);

        return popSet.stream().limit(n).map(i -> new ItemScore<>(i, (double) ratingsForItem.count(i))).collect(Collectors.toSet());
    }

    @Override
    public void build(Multimap<String, String> usersRatings) throws Exception {
        for (String user : usersRatings.keySet()) {
            for (String item : usersRatings.get(user)) {
                ratingsForItem.add(item);
            }
        }
    }
}
