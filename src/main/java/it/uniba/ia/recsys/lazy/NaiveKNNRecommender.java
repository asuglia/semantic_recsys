package it.uniba.ia.recsys.lazy;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.data.TransferData;
import it.uniba.ia.recsys.ItemRanker;
import it.uniba.ia.similarity.SimilarityEngine;
import it.uniba.ia.similarity.SimilarityMap;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

/**
 * Classe che implementa la strategia NaiveKNN
 */
public class NaiveKNNRecommender implements ItemRanker {
    private int k;
    private SimilarityEngine engine;
    private Multimap<String, String> usersRatings;
    private static final Logger logger = Logger.getLogger(WekaKNNRecommender.class.getName());
    private Multiset<String> itemUserPopularity;

    /**
     * Inizializza l'istanza corrente utilizzando i parametri specificati in input
     * <p>
     * Parametri struttura:
     * NEIGHBOURS - numero di simili da scegliere
     * SIM_ENGINE - gestore della matrice di similarità
     *
     * @param data struttura contenente i parametri
     */
    public NaiveKNNRecommender(TransferData data) {
        this.k = (int) data.get("NEIGHBOURS");
        this.engine = (SimilarityEngine) data.get("SIM_ENGINE");
    }

    /**
     * Inizializza gli score di popolarità da associare ad ogni item del dataset
     *
     * @param usersRatings espressione di gradimento degli utenti
     * @throws Exception errore nell'inizializzazione del modello
     */
    public void build(Multimap<String, String> usersRatings) throws Exception {
        this.usersRatings = usersRatings;
        this.itemUserPopularity = HashMultiset.create();

        // count how many time an item has been seen by a user
        for (String user : usersRatings.keySet()) {
            usersRatings.get(user).forEach(itemUserPopularity::add);
        }

    }

    /**
     * Produce le raccomandazioni per l'utente indicato secondo la strategia del NaiveKNN
     * @param user utente
     * @return lista di raccomandazioni
     * @throws Exception errore nella generazione delle raccomandazioni
     */
    public Set<ItemScore<String, Double>> rank(String user) throws Exception {
        Set<ItemScore<String, Double>> itemsList = new TreeSet<>();
        SimilarityMap<String, Double> simMap = engine.getSimilarityMap();
        Collection<String> ratedItems = usersRatings.get(user);
        int totUsers = usersRatings.keySet().size();

        if (ratedItems != null) {

            for (String ratedItem : ratedItems) {
                LinkedHashMap<String, Double> kNeigh = simMap.scores(ratedItem, k);
                if (kNeigh != null) {
                    kNeigh.keySet().stream().filter(newItem -> !ratedItems.contains(newItem)).forEach(newItem -> {
                        double popScore = Math.log10(totUsers / (itemUserPopularity.count(newItem) + 1)),
                                simScore = kNeigh.get(newItem);

                        itemsList.add(new ItemScore<>(newItem, popScore * simScore));
                    });

                }

            }
        } else {
            itemUserPopularity.stream().
                    forEach(i -> itemsList.add(
                            new ItemScore<>(i, Math.log10(totUsers / (itemUserPopularity.count(i) + 1)))));
        }

        return itemsList;
    }

}