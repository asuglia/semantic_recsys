package it.uniba.ia.recsys.lazy;

import com.google.common.collect.Multimap;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.data.TransferData;
import it.uniba.ia.recsys.ItemRanker;
import it.uniba.ia.similarity.SimilarityEngine;
import it.uniba.ia.similarity.WekaSimContext;
import it.uniba.ia.util.DatasetGenerator;
import weka.classifiers.lazy.IBk;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * #### IMPLEMENTAZIONE INCOMPLETA ####
 * Esecuzione di clustering sugli individui per la formazione di dataset da impiegare per poter
 * eseguire l'algoritmo KNN
 */
public class WekaKNNRecommender implements ItemRanker {
    private Map<String, IBk> usersClassifier;
    private int k;
    private SimilarityEngine engine;
    private Multimap<String, String> clusterizedItems;
    private static final Logger logger = Logger.getLogger(WekaKNNRecommender.class.getName());

    public WekaKNNRecommender(TransferData data) {
        this.k = (int) data.get("NEIGHBOURS");
        this.engine = (SimilarityEngine) data.get("SIM_ENGINE");
        this.clusterizedItems = (Multimap<String, String>) data.get("CLUSTERIZED_ITEMS");
    }

    @Override
    public void build(Multimap<String, String> usersRatings) throws Exception {
        usersClassifier = new HashMap<>();

        for (String user : usersRatings.keySet()) {
            // for each user build a WEKA_KNN classifier
            IBk knn = new IBk(k);
            knn.getNearestNeighbourSearchAlgorithm().setDistanceFunction(new WekaSimContext(engine));
            knn.buildClassifier(generateDatasetForUser(usersRatings.get(user)));
            usersClassifier.put(user, knn);
        }

    }

    @Override
    public Set<ItemScore<String, Double>> rank(String user) throws Exception {
        Set<ItemScore<String, Double>> newItems = new TreeSet<>();

        return newItems;
    }

    private Instances generateDatasetForUser(Collection<String> ratedItems) {
        Attribute itemsAttr = new Attribute("items", new ArrayList<>(new HashSet<>(clusterizedItems.values())));

        ArrayList<String> classValues = new ArrayList<>();
        classValues.add("0");
        classValues.add("1");
        Attribute classAttr = new Attribute("likes", classValues);

        ArrayList<Attribute> attrInfo = new ArrayList<>();

        attrInfo.add(itemsAttr);
        attrInfo.add(classAttr);

        Instances dataset = new Instances("ratings", attrInfo, 0);
        List<String> posItems = new ArrayList<>();

        for (String item : ratedItems) {
            getItemsInCluster(item).stream().forEach(currItem -> {
                        Instance inst = new DenseInstance(2);
                        inst.setDataset(dataset);
                        inst.setValue(0, currItem);
                        inst.setClassValue("1"); // positive instance
                        dataset.add(inst);
                        posItems.add(currItem);
                    }
            );

            Instance inst = new DenseInstance(2);
            inst.setDataset(dataset);
            inst.setValue(0, item);
            inst.setClassValue("1"); // positive instance
            dataset.add(inst);
            posItems.add(item);
        }


        clusterizedItems.values().
                stream().
                filter(item -> !posItems.contains(item)).forEach(
                item -> {
                    Instance inst = new DenseInstance(2);
                    inst.setDataset(dataset);
                    inst.setValue(0, item);
                    inst.setClassValue("0"); // negative instance
                    dataset.add(inst);
                }
        );

        return dataset;

    }

    private List<String> getItemsInCluster(String item) {
        List<String> items = new ArrayList<>();

        for (String i : clusterizedItems.keySet()) {
            // "item" is contained in the cluster whose medoid is "i"
            if (clusterizedItems.containsEntry(i, item)) {
                // get all the other items
                items.addAll(clusterizedItems.get(i).
                        stream().
                        filter(currItem -> !currItem.equals(item)).
                        collect(Collectors.toList()));
                // finally add also the medoid
                items.add(i);
            }
        }

        return items;
    }


}
