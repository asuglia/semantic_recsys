package it.uniba.ia.sparql;

import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by asuglia on 5/21/15.
 */
public class SPARQLReader {
    private static final Logger log = Logger.getLogger(SPARQLReader.class.getSimpleName());

    public List<QuerySolution> getEntityDataFromEndpoint(String endpoint, String resourceQuery) {
        List<QuerySolution> entityData = new ArrayList<>();
        try {
            Query q = QueryFactory.create(resourceQuery);

            log.info("Executing query: " + resourceQuery);

            QueryExecution queryExec = QueryExecutionFactory.sparqlService(endpoint, q);

            ResultSet results = queryExec.execSelect();

            results.forEachRemaining(entityData::add);

        } catch (Exception e) {
            log.severe(e.toString());
        }
        return entityData;

    }

    public Model describeEntity(String endpoint, String resourceIRI) {
        String resourceQuery = "DESCRIBE ?s " +
                "WHERE{ VALUES ?s { <" + resourceIRI + "> }. ?s ?p ?o}";
        Model data = null;

        try {
            Query q = QueryFactory.create(resourceQuery);

            log.info("Executing query: " + resourceQuery);

            QueryExecution queryExec = QueryExecutionFactory.sparqlService(endpoint, q);

            data = queryExec.execDescribe();
        } catch (Exception e) {
            log.severe(e.toString());
        }

        return data;
    }

    public Model constructEntity(String endpoint, String resourceIRI) {
        String resourceQuery =
                "PREFIX rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n" +
                        "PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#> \n" +
                        "PREFIX owl:     <http://www.w3.org/2002/07/owl#>" +
                        "CONSTRUCT \n" +
                        "  { ?p rdfs:subClassOf ?supertype .\n" +
                        "    ?p rdf:type owl:Class .\n" +
                        "    ?supertype rdf:type owl:Class .}\n" +
                        "WHERE\n" +
                        "  { <" + resourceIRI + "> rdf:type ?p .\n" +
                        "    ?p (rdfs:subClassOf)* ?supertype\n" +
                        "  }";

        Model schema = null;

        try {
            Query q = QueryFactory.create(resourceQuery);

            log.info("Executing query: " + resourceQuery);

            QueryExecution queryExec = QueryExecutionFactory.sparqlService(endpoint, q);

            schema = queryExec.execConstruct();

        } catch (Exception e) {
            log.severe(e.toString());
        }

        return schema;
    }

    public Model getModelFromURI(String uri) {
        return RDFDataMgr.loadModel(uri, Lang.RDFXML);

    }

}
