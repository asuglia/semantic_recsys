package it.uniba.ia;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import it.uniba.ia.data.ConceptTaxonomyScorer;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectOutputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Entry point per l'esecuzione dell'algoritmo per il calcolo dei pesi da associare ad ogni feature
 */
public class MainTaxonomyScorer {
    private static final Logger logger = Logger.getLogger(MainTaxonomyScorer.class.getName());

    /**
     * Avvia l'esecuzione del calcolo dei pesi da associare alle feature del contesto
     * utilizzando dei parametri specificati nel file di Properties indicato
     *
     * @param args path del file di properties (vedi concept_scorer.prop)
     */
    public static void main(String[] args) {
        if (args.length == 1) {
            Properties prop = new Properties();
            try {
                prop.load(new FileReader(args[0]));
                String ontologyFile = prop.getProperty("ontology_file"),
                        conceptScoresFile = prop.getProperty("concept_scores_file");

                OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
                OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(ontologyFile));
                PelletReasoner reasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);

                reasoner.prepareReasoner();

                logger.info("Reasoner is ready!");

                ConceptTaxonomyScorer scorer = new ConceptTaxonomyScorer();
                scorer.computeScore(reasoner);

                logger.info("Computed concepts' scores");

                ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(conceptScoresFile));
                stream.writeObject(scorer);


                logger.info("Serialized concepts' scores");


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("Incorrect number of parameters: #program_name <properties_file>");
        }


    }
}
