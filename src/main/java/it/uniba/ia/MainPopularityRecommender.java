package it.uniba.ia;

import com.google.common.collect.BiMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.data.TransferData;
import it.uniba.ia.recsys.ItemRanker;
import it.uniba.ia.recsys.RecommenderFactory;
import it.uniba.ia.similarity.SimilarityEngine;
import it.uniba.ia.util.ESWCDataManager;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by asuglia on 8/6/15.
 */
public class MainPopularityRecommender {
    private static final Logger logger = Logger.getLogger(MainPopularityRecommender.class.getName());

    public static void main(String[] args) {
        if (args.length == 1) {
            Properties prop = new Properties();
            try {
                prop.load(new FileReader(args[0]));
                String trainFormat = prop.getProperty("train_format"),
                        testFormat = prop.getProperty("test_format"),
                        outputFormat = prop.getProperty("output_format");

                int folds = Integer.parseInt(prop.getProperty("folds"));
                int topN = Integer.parseInt(prop.getProperty("top_n"));


                for (int i = 1; i <= folds; i++) {
                    Multimap<String, String> trainRatings = ESWCDataManager.readRatings(String.format(trainFormat, i), '\t'),
                            testRatings = ESWCDataManager.readRatings(String.format(testFormat, i), '\t');

                    logger.info("Fold " + i);


                    ItemRanker ranker = RecommenderFactory.create(new TransferData(
                            new String[]{
                                    "RECTYPE",
                                    "TOP_N"
                            },
                            new Object[]{
                                    RecommenderFactory.RecType.POPULAR,
                                    topN,
                            }
                    ));


                    ranker.build(trainRatings);

                    logger.info("Recommender build for fold " + i);

                    Map<String, Set<ItemScore<String, Double>>> userRec = generateRecommendations(ranker, testRatings.keySet());
                    serializeRecommendations(String.format(outputFormat, i), userRec, topN);

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("Incorrect number of parameters: #program_name <properties_file>");
        }


    }

    private static Map<String, Set<ItemScore<String, Double>>> generateRecommendations(ItemRanker ranker, Set<String> users) throws Exception {
        Map<String, Set<ItemScore<String, Double>>> recommendations = new HashMap<>();

        for (String user : users) {
            recommendations.put(user, ranker.rank(user));
            logger.info("Generated recommendations for user " + user);
        }

        return recommendations;
    }

    private static void serializeRecommendations(String outputFile,
                                                 Map<String, Set<ItemScore<String, Double>>> recommendations,
                                                 int topN) throws IOException {
        try (CSVPrinter printer = new CSVPrinter(new FileWriter(outputFile), CSVFormat.TDF)) {
            for (String user : recommendations.keySet()) {
                Set<ItemScore<String, Double>> recList = recommendations.get(user);

                for (ItemScore<String, Double> item : Iterables.limit(recList, topN)) {
                    printer.printRecord(user, item.getItem(), 1d);
                }
            }
        }

    }

}
