package it.uniba.ia;


import com.google.common.collect.BiMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.data.TransferData;
import it.uniba.ia.recsys.ItemRanker;
import it.uniba.ia.recsys.RecommenderFactory;
import it.uniba.ia.similarity.SimilarityEngine;
import it.uniba.ia.util.ESWCDataManager;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Entry point per l'esecuzione dell'algoritmo NaiveKNN
 */
public class MainRecommenderKNN {
    private static final Logger logger = Logger.getLogger(MainRecommenderKNN.class.getName());

    /**
     * Genera le raccomandazioni usando l'algoritmo NaiveKNN impiegando come
     * parametri quelli specificati nel file di Properties specificato
     *
     * @param args path del file di properties - vedi rec_knn.prop
     */
    public static void main(String[] args) {
        if (args.length == 1) {
            Properties prop = new Properties();
            try {
                prop.load(new FileReader(args[0]));
                String trainFormat = prop.getProperty("train_format"),
                        testFormat = prop.getProperty("test_format"),
                        itemInfoFile = prop.getProperty("item_file_info"),
                        entityName = prop.getProperty("entity_name"),
                        simScoresFile = prop.getProperty("sim_scores"),
                        outputFormat = prop.getProperty("output_format"),
                        kString = prop.getProperty("k");

                String[] kVals = kString.split(",");
                int folds = Integer.parseInt(prop.getProperty("folds"));
                int topN = Integer.parseInt(prop.getProperty("top_n"));
                BiMap<String, String> itemInfo = ESWCDataManager.readResources(itemInfoFile, entityName);
                SimilarityEngine engine = new SimilarityEngine();
                engine.load(simScoresFile);

                for (String kS : kVals) {
                    int k = Integer.parseInt(kS);
                    for (int i = 1; i <= folds; i++) {
                        Multimap<String, String> trainRatings = ESWCDataManager.readRatings(String.format(trainFormat, i), '\t'),
                                testRatings = ESWCDataManager.readRatings(String.format(testFormat, i), '\t');

                        logger.info("Fold " + i);


                        ItemRanker ranker = RecommenderFactory.create(new TransferData(
                                new String[]{
                                        "RECTYPE",
                                        "NEIGHBOURS",
                                        "SIM_ENGINE",
                                        "ITEM_INFO"
                                },
                                new Object[]{
                                        RecommenderFactory.RecType.NAIVE_KNN,
                                        k,
                                        engine,
                                        itemInfo
                                }
                        ));


                        ranker.build(trainRatings);

                        logger.info("Recommender build for fold " + i);

                        Map<String, Set<ItemScore<String, Double>>> userRec = generateRecommendations(ranker, testRatings.keySet());
                        serializeRecommendations(String.format(outputFormat, k, i), userRec, topN);

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("Incorrect number of parameters: #program_name <properties_file>");
        }


    }

    private static Map<String, Set<ItemScore<String, Double>>> generateRecommendations(ItemRanker ranker, Set<String> users) throws Exception {
        Map<String, Set<ItemScore<String, Double>>> recommendations = new HashMap<>();

        for (String user : users) {
            recommendations.put(user, ranker.rank(user));
            logger.info("Generated recommendations for user " + user);
        }

        return recommendations;
    }

    private static void serializeRecommendations(String outputFile,
                                                 Map<String, Set<ItemScore<String, Double>>> recommendations,
                                                 int topN) throws IOException {
        try (CSVPrinter printer = new CSVPrinter(new FileWriter(outputFile), CSVFormat.TDF)) {
            for (String user : recommendations.keySet()) {
                Set<ItemScore<String, Double>> recList = recommendations.get(user);

                for (ItemScore<String, Double> item : Iterables.limit(recList, topN)) {
                    printer.printRecord(user, item.getItem(), 1d);
                }
            }
        }

    }


}
