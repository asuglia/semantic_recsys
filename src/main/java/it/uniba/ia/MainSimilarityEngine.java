package it.uniba.ia;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import com.google.common.collect.BiMap;
import it.uniba.ia.data.TransferData;
import it.uniba.ia.similarity.SimilarityEngine;
import it.uniba.ia.similarity.SimilarityMap;
import it.uniba.ia.util.ESWCDataManager;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Entry point per l'esecuzione del calcolo delle similarità fra gli item del dataset
 */
public class MainSimilarityEngine {
    private static final Logger logger = Logger.getLogger(MainTaxonomyScorer.class.getName());

    /**
     * Avvia l'esecuzione del calcolo delle similarità utilizzando dei parametri specificati
     * nel file di Properties indicato
     *
     * @param args path del file di properties (vedi sim_scores.prop)
     */
    public static void main(String[] args) {
        if (args.length == 1) {
            Properties prop = new Properties();
            try {
                prop.load(new FileReader(args[0]));
                String ontologyFile = prop.getProperty("ontology_file"),
                        conceptScoresFile = prop.getProperty("concept_scores_file"),
                        simScoresFile = prop.getProperty("sim_scores_file"),
                        entityName = prop.getProperty("entity_name"),
                        individualsInfoFile = prop.getProperty("item_info_file");

                OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
                OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(ontologyFile));
                PelletReasoner reasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);
                BiMap<String, String> itemInfo = ESWCDataManager.readResources(individualsInfoFile, entityName);

                reasoner.prepareReasoner();

                logger.info("Reasoner is ready!");


                SimilarityEngine engine = new SimilarityEngine(
                        new TransferData(
                                new String[]{"REASONER", "CONCEPT_SCORES_FILE", "ITEM_INFO"},
                                new Object[]{
                                        reasoner,
                                        conceptScoresFile,
                                        itemInfo
                                }
                        )
                );
                engine.compute();

                engine.serialize(simScoresFile);
                logger.info("Serialized concepts' scores");
                /*
                BiMap<String, String> itemInfo = ESWCDataManager.readResources(individualsInfoFile, entityName);
                SimilarityEngine engine = new SimilarityEngine();

                engine.load("/home/asuglia/dataset/eswc2015/book/sim_scores.score");

                SimilarityMap<String, Double> simMap = engine.getSimilarityMap();

                System.out.println(simMap.get("bo900", "bo831"));
                System.out.println(simMap.get("bo831", "bo900"));
*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("Incorrect number of parameters: #program_name <properties_file>");
        }


    }
}
