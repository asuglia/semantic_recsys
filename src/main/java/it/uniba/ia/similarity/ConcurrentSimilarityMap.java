package it.uniba.ia.similarity;

import com.google.common.collect.BiMap;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.util.ESWCDataManager;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Classe che definisce una matrice di similarità che supporta l'accesso concorrente
 */
public class ConcurrentSimilarityMap<K extends Comparable<K>, S extends Comparable<S>> implements SimilarityMap<K, S>, Serializable {
    private static final long serialVersionUID = 6192644067268759497L;
    private Map<K, ScoresMap<K, S>> simMap;
    private boolean simmetricMatrix;

    public ConcurrentSimilarityMap() {
        this.simMap = new HashMap<>();
    }

    /**
     * Inizializza la matrice indicando se è simmetrica
     *
     * @param simmetric matrice simmetrica
     */
    public ConcurrentSimilarityMap(boolean simmetric) {
        this.simmetricMatrix = simmetric;
        this.simMap = new HashMap<>();
    }


    public synchronized void put(K a, K b, S score) {
        if (simmetricMatrix) {

            // The current similarity value is not present
            // after the insertion will be present for both of them
            if (get(a, b) == null) {
                ScoresMap<K, S> scoresA = simMap.get(a);
                if (scoresA == null) {
                    scoresA = new ScoresMap<>();
                    simMap.put(a, scoresA);
                }

                scoresA.put(b, score);

            } // put() conventionally replaces the value
            else {
                ScoresMap<K, S> scoresA = simMap.get(a),
                        scoresB = simMap.get(b);

                if (scoresA != null && scoresA.get(b) != null) {
                    scoresA.put(b, score);
                }

                if (scoresB != null && scoresB.get(a) != null) {
                    scoresB.put(a, score);
                }
            }

        } else {
            ScoresMap<K, S> scores = simMap.get(a);
            if (scores == null) {
                scores = new ScoresMap<>();
                simMap.put(a, scores);
            }

            scores.put(b, score);
        }

    }

    @Override
    public void putAll(K a, Map<K, S> scores) {
        scores.keySet().stream().forEach(b -> put(a, b, scores.get(b)));
    }


    @Override
    public LinkedHashMap<K, S> scores(K a) {
        ScoresMap<K, S> map = simMap.get(a);

        if (map != null) {
            return map.orderedScores();
        }


        // map is null. Construct the scores map from the other similarity values
        Set<ItemScore<K, S>> scoreSet = simMap.keySet().stream().
                filter(item -> !a.equals(item)).
                map(item -> new ItemScore<>(item, get(item, a))).
                collect(Collectors.toSet());

        LinkedHashMap<K, S> returnMap = new LinkedHashMap<>();

        scoreSet.stream().
                filter(i -> i.getScore() != null).
                forEach(i -> returnMap.put(i.getItem(), i.getScore()));

        return returnMap;

    }


    @Override
    public LinkedHashMap<K, S> scores(K a, int k) {
        ScoresMap<K, S> map = simMap.get(a);

        if (map != null) {
            return map.orderedScores(k);
        }


        // map is null. Construct the scores map from the other similarity values
        Set<ItemScore<K, S>> scoreSet = simMap.keySet().stream().
                filter(item -> !a.equals(item)).
                map(item -> new ItemScore<>(item, get(item, a))).
                collect(Collectors.toSet());

        LinkedHashMap<K, S> returnMap = new LinkedHashMap<>();

        scoreSet.stream().
                filter(i -> i.getScore() != null).
                limit(k).
                forEach(i -> returnMap.put(i.getItem(), i.getScore()));

        return returnMap;

    }


    @Override
    public void load(String inputFile) throws SimilarityMapException {
        try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(inputFile))) {
            this.simMap = (Map<K, ScoresMap<K, S>>) stream.readObject();
            this.simmetricMatrix = stream.readBoolean();
        } catch (ClassNotFoundException | IOException e) {
            throw new SimilarityMapException(e.getLocalizedMessage());
        }
    }

    @Override
    public void serialize(String outputFile) throws SimilarityMapException {
        try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(outputFile))) {
            stream.writeObject(simMap);
            stream.writeBoolean(simmetricMatrix);
        } catch (IOException e) {
            throw new SimilarityMapException(e.toString());
        }
    }

    public S get(K a, K b) {
        if (simmetricMatrix) {
            Map<K, S> firstMap = simMap.get(a);
            if (firstMap != null) {
                S first = firstMap.get(b);

                if (first != null)
                    return first;
            }

            Map<K, S> secMap = simMap.get(b);
            if (secMap != null) {
                S sec = secMap.get(a);

                if (sec != null)
                    return sec;
            }

            return null;
        }

        return this.simMap.get(a).get(b);

    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(this.simMap);
        out.writeBoolean(this.simmetricMatrix);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.simMap = (Map<K, ScoresMap<K, S>>) in.readObject();
        this.simmetricMatrix = in.readBoolean();
    }

    public void clear() {
        simMap.clear();
    }

    public String toString() {
        return this.simMap.toString();
    }

    public static void main(String[] args) throws SimilarityMapException, IOException {
        SimilarityEngine engine = new SimilarityEngine();

        engine.load("/home/asuglia/dataset/eswc2015/book/sim_scores.score");
        BiMap<String, String> itemInfo = ESWCDataManager.readResources("/home/asuglia/dataset/eswc2015/book/items_books.dat", "book");

        SimilarityMap<String, Double> similarityMap = engine.getSimilarityMap();

        for(String item : itemInfo.keySet()) {
            LinkedHashMap<String, Double> scores = similarityMap.scores(item, 1);

            if(scores.isEmpty()) {
                System.out.println("No scores for item " + item);
            }

        }

    }
}
