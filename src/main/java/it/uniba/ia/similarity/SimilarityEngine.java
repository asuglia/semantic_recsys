package it.uniba.ia.similarity;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.google.common.collect.BiMap;
import it.uniba.ia.data.Context;
import it.uniba.ia.data.TransferData;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Classe che calcola le similarità tra tutti gli individui
 */
public class SimilarityEngine {
    private static final Logger logger = Logger.getLogger(SimilarityEngine.class.getName());
    private SimilarityMap<String, Double> individualSims;
    private Similarity simFunction;
    private Set<OWLNamedIndividual> individuals;
    private BiMap<String, String> itemInfo;


    /**
     * Inizializza l'engine
     */
    public SimilarityEngine() {

    }

    /**
     * Inizializza l'engine impiegando i parametri specificati nella struttura in input.
     * <p>
     * Parametri struttura:
     * REASONER - istanza del reasoner Pellet
     * CONCEPT_SCORES_FILE - path del file che contiene gli score da associare ad ogni feature
     * ITEM_INFO - informazioni associate ad ogni item del dataset
     *
     * @param data struttura contenente i parametri
     */
    public SimilarityEngine(TransferData data) {
        individualSims = new ConcurrentSimilarityMap<>(true);
        PelletReasoner reasoner = (PelletReasoner) data.get("REASONER");
        String conceptScoresFile = (String) data.get("CONCEPT_SCORES_FILE");
        itemInfo = (BiMap<String, String>) data.get("ITEM_INFO");

        retrieveIndividuals(reasoner);

        if (conceptScoresFile != null)
            generateSimilarityFunction(reasoner, conceptScoresFile);
        else
            generateSimilarityFunction(reasoner);

    }

    private void retrieveIndividuals(PelletReasoner reasoner) {
        OWLOntologyManager manager = reasoner.getRootOntology().getOWLOntologyManager();
        OWLDataFactory df = manager.getOWLDataFactory();

        individuals = itemInfo.inverse().keySet().stream().
                map(iri -> df.getOWLNamedIndividual(IRI.create(iri))).
                collect(Collectors.toSet());
    }

    private void generateSimilarityFunction(PelletReasoner reasoner) {
        simFunction = SimilarityFactory.create(
                new TransferData(
                        new String[]{"SIM_FUNCTION", "CONTEXT", "REASONER"},
                        new Object[]{
                                SimilarityFunctions.SIMILARITY_CONTEXT,
                                new Context(reasoner, itemInfo),
                                reasoner
                        }));


    }

    private void generateSimilarityFunction(PelletReasoner reasoner,
                                            String conceptScoresFile) {
        simFunction = SimilarityFactory.create(
                new TransferData(
                        new String[]{"SIM_FUNCTION", "CONTEXT", "REASONER", "CONCEPT_SCORES_FILE"},
                        new Object[]{
                                SimilarityFunctions.SIMILARITY_CONTEXT,
                                new Context(reasoner, itemInfo),
                                reasoner,
                                conceptScoresFile
                        }));


    }

    /**
     * Restituisce la matrice di similarità calcolata
     *
     * @return matrice di similarità
     */
    public SimilarityMap<String, Double> getSimilarityMap() {
        return individualSims;
    }

    /**
     * Avvia il calcolo, in parallelo, della matrice di similarità
     */
    public void compute() {
        if (individualSims != null && individuals != null) {
            // Remove previously computed similarities
            individualSims.clear();

            individuals.parallelStream().
                    forEach(a ->
                                    individuals.parallelStream().
                                            filter(b -> {
                                                String aIRI = a.toStringID(),
                                                        bIRI = b.toStringID(),
                                                        idA = itemInfo.inverse().get(aIRI),
                                                        idB = itemInfo.inverse().get(bIRI);

                                                return !aIRI.equals(bIRI) && individualSims.get(idA, idB) == null;

                                            }).
                                            forEach(b -> {
                                                String aIRI = a.toStringID(),
                                                        bIRI = b.toStringID(),
                                                        idA = itemInfo.inverse().get(aIRI),
                                                        idB = itemInfo.inverse().get(bIRI);

                                                individualSims.put(idA, idB, simFunction.score(a, b));
                                            })

                    );
        }
    }

    /**
     * Carica l'engine a partire dal file specificato
     * @param inputFile path del file contenente i dati dell'engine
     * @throws SimilarityMapException errore nel caricamento dei dati dell'engine
     */
    public void load(String inputFile) throws SimilarityMapException {
        logger.info("Loading similarity scores from file: " + inputFile);
        individualSims = new ConcurrentSimilarityMap<>(true);
        individualSims.load(inputFile);

    }

    /**
     * Serializza i dati dell'engine nel file specificato
     * @param outFile path del file che conterrà i dati dell'engine
     * @throws SimilarityMapException errore nella serializzazione
     */
    public void serialize(String outFile) throws SimilarityMapException {
        logger.info("Writing similarity scores to file: " + outFile);

        individualSims.serialize(outFile);
    }
}
