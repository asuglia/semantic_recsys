package it.uniba.ia.similarity;

import org.semanticweb.owlapi.model.OWLIndividual;

/**
 * Created by asuglia on 5/21/15.
 */
public interface Similarity {
    public double score(OWLIndividual a, OWLIndividual b);
}
