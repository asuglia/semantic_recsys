package it.uniba.ia.similarity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by asuglia on 6/26/15.
 */
public interface SimilarityMap<K extends Comparable<K>, S extends Comparable<S>> {
    /**
     * Cancella il contenuto della matrice
     */
    void clear();

    /**
     * Restituisce lo score associato alla coppia di identificativi (a,b)
     *
     * @param a primo identificativo
     * @param b secondo identificativo
     * @return score
     */
    S get(K a, K b);

    /**
     * Inserisce uno score per la coppia (a,b) in maniera concorrente
     *
     * @param a primo indentificativo
     * @param b secondo identificativo
     * @param score score associato alla coppia
     */
    void put(K a, K b, S score);

    void putAll(K a, Map<K, S> scores);

    /**
     * Restituisce gli score associati agli item per i quali vi è uno score con
     * l'identificativo specificato
     *
     * @param a identificativo
     * @return lista di score
     */
    LinkedHashMap<K, S> scores(K a);

    /**
     * Restituisce gli score associati a "k" item per i quali vi è uno score con
     * l'identificativo specificato
     *
     * @param a identificativo
     * @return lista di k score
     */
    LinkedHashMap<K, S> scores(K a, int k);

    /**
     * Legge i dati della matrice di simialarità a partire
     * @param inputFile
     * @throws SimilarityMapException
     */
    void load(String inputFile) throws SimilarityMapException;

    void serialize(String outputFile) throws SimilarityMapException;

}
