package it.uniba.ia.similarity;

import com.google.common.collect.Iterables;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

/**
 * Classe che rappresenta una struttura dati simile ad un dizionario che memorizza le coppie
 * sulla base del valore e non rispetto alla chiave
 */
class ScoresMap<K extends Comparable<K>, S extends Comparable<S>> implements Map<K, S>, Serializable {
    private static final long serialVersionUID = 889371778458534743L;
    private Set<K> orderedID;
    private Map<K, S> scores;


    public ScoresMap() {
        scores = new HashMap<>();
        orderedID = new TreeSet<>(new ScoresComparator<>());

    }

    private class ScoresComparator<K extends Comparable<K>> implements Comparator<K>, Serializable {

        private static final long serialVersionUID = -443760041832374962L;

        @Override
        public int compare(K o1, K o2) {
            int tempComp = scores.get(o1).compareTo(scores.get(o2));

            return tempComp != 0 ? tempComp : o1.compareTo(o2);
        }
    }

    @Override
    public int size() {
        return scores.size();
    }

    @Override
    public boolean isEmpty() {
        return scores.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return scores.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return scores.containsValue(value);
    }

    @Override
    public S get(Object key) {
        return scores.get(key);
    }

    @Override
    public S put(K key, S value) {
        if (scores.containsKey(key))
            orderedID.remove(key);
        S prevValue = scores.put(key, value);
        orderedID.add(key);

        return prevValue;
    }

    @Override
    public S remove(Object key) {
        orderedID.remove(key);
        return scores.remove(key);

    }

    @Override
    public void putAll(Map<? extends K, ? extends S> m) {
        for (K key : m.keySet()) {
            this.put(key, m.get(key));
        }
    }

    @Override
    public void clear() {
        scores.clear();
        orderedID.clear();
    }

    @Override
    public Set<K> keySet() {

        return Collections.unmodifiableSet(scores.keySet());
    }

    @Override
    public Collection<S> values() {
        return Collections.unmodifiableCollection(scores.values());
    }

    @Override
    public Set<Entry<K, S>> entrySet() {
        return Collections.unmodifiableSet(scores.entrySet());
    }

    public LinkedHashMap<K, S> orderedScores(int k) {
        LinkedHashMap<K, S> returnScores = new LinkedHashMap<>(k);

        for (K id : Iterables.limit(orderedID, k)) {
            returnScores.put(id, scores.get(id));
        }

        return returnScores;
    }

    /**
     * Restituisce la lista ordinata di score di similarità
     *
     * @return punteggio associato ad ogni item
     */
    public LinkedHashMap<K, S> orderedScores() {
        LinkedHashMap<K, S> returnScores = new LinkedHashMap<>();

        for (K id : orderedID) {
            returnScores.put(id, scores.get(id));
        }

        return returnScores;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;

        if (!(o instanceof ScoresMap))
            return false;

        ScoresMap<K, S> scoreMap = (ScoresMap<K, S>) o;

        return this.scores.equals(scoreMap.scores);
    }

    @Override
    public int hashCode() {
        return scores.hashCode();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(this.scores);
        out.writeObject(this.orderedID);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.scores = (Map<K, S>) in.readObject();
        this.orderedID = (Set<K>) in.readObject();
    }


    public String toString() {
        return orderedScores().toString();
    }

    public static void main(String[] args) {
        ScoresMap<String, Double> scores = new ScoresMap<>();

        scores.put("u1", 0.5);
        scores.put("u2", 0.4);
        scores.put("u3", 0.2);
        scores.put("u1", 0.1);


        System.out.println(scores);


    }
}