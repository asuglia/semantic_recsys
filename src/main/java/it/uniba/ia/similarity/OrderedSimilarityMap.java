package it.uniba.ia.similarity;

import it.uniba.ia.data.ItemScore;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Classe che implementa una semplice matrice che mantiene ordinati gli score associati ad un certo item
 */
public class OrderedSimilarityMap<K extends Comparable<K>, S extends Comparable<S>> implements SimilarityMap<K, S>, Serializable {
    private static final long serialVersionUID = 3777414237080258239L;
    private Map<K, ScoresMap<K, S>> simMap;
    private boolean simmetricMatrix;

    public OrderedSimilarityMap() {
        this.simMap = new HashMap<>();
    }

    public OrderedSimilarityMap(boolean simmetric) {
        this.simmetricMatrix = simmetric;
        this.simMap = new HashMap<>();
    }

    public void put(K a, K b, S score) {
        if (simmetricMatrix) {
            ScoresMap<K, S> scoresA = simMap.get(a),
                    scoresB = simMap.get(b);
            // The current similarity value is not present
            // after the insertion will be present for both of them
            if (get(a, b) == null) {
                if (scoresA == null) {
                    scoresA = new ScoresMap<>();
                    simMap.put(a, scoresA);
                }

                if (scoresB == null) {
                    scoresB = new ScoresMap<>();
                    simMap.put(b, scoresB);
                }

            } // put() conventionally replaces the value

            scoresA.put(b, score);
            scoresB.put(a, score);

        } else {
            ScoresMap<K, S> scores = simMap.get(a);
            if (scores == null) {
                scores = new ScoresMap<>();
                simMap.put(a, scores);
            }

            scores.put(b, score);
        }

    }

    @Override
    public void putAll(K a, Map<K, S> scores) {
        scores.keySet().stream().forEach(b -> put(a, b, scores.get(b)));
    }

    @Override
    public LinkedHashMap<K, S> scores(K a) {
        ScoresMap<K, S> map = simMap.get(a);

        if (map != null) {
            return map.orderedScores();
        }


        // map is null. Construct the scores map from the other similarity values
        Set<ItemScore<K, S>> scoreSet = simMap.keySet().stream().
                filter(item -> !a.equals(item)).
                map(item -> new ItemScore<>(item, get(item, a))).
                collect(Collectors.toSet());

        LinkedHashMap<K, S> returnMap = new LinkedHashMap<>();

        scoreSet.stream().
                filter(i -> i.getScore() != null).
                forEach(i -> returnMap.put(i.getItem(), i.getScore()));

        return returnMap;

    }

    @Override
    public LinkedHashMap<K, S> scores(K a, int k) {
        ScoresMap<K, S> map = simMap.get(a);

        if (map != null) {
            return map.orderedScores(k);
        }


        // map is null. Construct the scores map from the other similarity values
        Set<ItemScore<K, S>> scoreSet = simMap.keySet().stream().
                filter(item -> !a.equals(item)).
                map(item -> new ItemScore<>(item, get(item, a))).
                collect(Collectors.toSet());

        LinkedHashMap<K, S> returnMap = new LinkedHashMap<>();

        scoreSet.stream().
                filter(i -> i.getScore() != null).
                limit(k).
                forEach(i -> returnMap.put(i.getItem(), i.getScore()));

        return returnMap;

    }

    @Override
    public void load(String inputFile) throws SimilarityMapException {
        try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(inputFile))) {
            this.simMap = (Map<K, ScoresMap<K, S>>) stream.readObject();
            this.simmetricMatrix = stream.readBoolean();

        } catch (ClassNotFoundException | IOException e) {
            throw new SimilarityMapException(e.getLocalizedMessage());
        }
    }

    @Override
    public void serialize(String outputFile) throws SimilarityMapException {
        try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(outputFile))) {
            stream.writeObject(simMap);
            stream.writeBoolean(simmetricMatrix);

        } catch (IOException e) {
            throw new SimilarityMapException(e.toString());
        }
    }

    @Override
    public S get(K a, K b) {
        Map<K, S> firstMap = simMap.get(a);
        if (firstMap != null) {
            S first = firstMap.get(b);

            if (first != null)
                return first;
        }

        Map<K, S> secMap = simMap.get(b);
        if (secMap != null) {
            S sec = secMap.get(b);

            if (sec != null)
                return sec;
        }

        return null;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(this.simMap);
        out.writeBoolean(this.simmetricMatrix);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.simMap = (Map<K, ScoresMap<K, S>>) in.readObject();
        this.simmetricMatrix = in.readBoolean();
    }

    public void clear() {
        simMap.clear();
    }

    public String toString() {
        return this.simMap.toString();
    }

    public static void main(String[] args) {
        OrderedSimilarityMap<String, Double> simMap = new OrderedSimilarityMap<>(true);

        simMap.put("u1", "u2", 0.4);
        simMap.put("u1", "u3", 0.1);
        simMap.put("u1", "u4", 0.5);
        simMap.put("u2", "u4", 0.2);
        simMap.put("u3", "u4", 0.2);

        System.out.println(simMap.get("u1", "u2"));
        System.out.println(simMap.get("u2", "u1"));

        System.out.println(simMap.scores("u1"));
        System.out.println(simMap.scores("u4"));

    }
}
