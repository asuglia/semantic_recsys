package it.uniba.ia.similarity;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import it.uniba.ia.data.ConceptTaxonomyScorer;
import it.uniba.ia.data.Context;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.reasoner.NodeSet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Logger;


/**
 * Implementazione della misura di similarità chiamata Similarity in Context (Fanizzi et al)
 */
class SimilarityInContext implements Similarity {
    private Context context;
    private PelletReasoner reasoner;
    private static final double UNCERTAIN_VALUE = 0.5;
    private static final int PINDEX = 2;
    private static final Logger logger = Logger.getLogger(SimilarityInContext.class.getName());
    private ConceptTaxonomyScorer conceptScorer;
    private final Map<OWLClassExpression, HashSet<OWLIndividual>> conceptAssertions;

    /**
     * Inizializza i parametri necessari al calcolo della misura di similarità
     * specificati in input.
     *
     * @param context  contesto di riferimento
     * @param reasoner Pellet reasoner
     */
    public SimilarityInContext(Context context, PelletReasoner reasoner) {
        this.context = context;
        this.reasoner = reasoner;
        this.conceptScorer = new ConceptTaxonomyScorer();
        this.conceptScorer.computeScore(reasoner);
        this.conceptAssertions = new HashMap<>();
    }

    /**
     * Inizializza i parametri necessari al calcolo della misura di similarità
     * specificati in input.
     *
     * @param context contesto di riferimento
     * @param reasoner Pellet reasoner
     * @param conceptScorerFile file contenente i pesi associati alle feature del contesto
     */
    public SimilarityInContext(Context context, PelletReasoner reasoner, String conceptScorerFile) {
        this.context = context;
        this.reasoner = reasoner;
        readConceptScorer(conceptScorerFile);
        this.conceptAssertions = new HashMap<>();
    }

    private void readConceptScorer(String conceptScorerFile) {
        try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(conceptScorerFile))) {
            conceptScorer = (ConceptTaxonomyScorer) stream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Restituisce lo score associato a due individui
     *
     * @param a primo individuo
     * @param b secondo individuo
     * @return similarità tra a e b
     */
    public double score(OWLIndividual a, OWLIndividual b) {
        return score(a, b, PINDEX);
    }

    private double score(OWLIndividual a, OWLIndividual b, int p) {
        logger.info("Similarity score: " + a + " - " + b);
        double finalScore = 0d;

        for (OWLClass feature : context.getFeatureList()) {
            double tempScore =
                    (conceptScorer.get(feature) * Math.pow(Math.abs(piFunction(feature, a) - piFunction(feature, b)), p));
            finalScore += tempScore;
        }

        // It's a distance function
        return Math.pow(finalScore, (double) 1 / p);
    }

    private double piFunction(OWLClass feature, OWLIndividual individual) {
        HashSet<OWLIndividual> currFeatureIndividuals = conceptAssertions.get(feature);
        if (currFeatureIndividuals == null) {
            NodeSet<OWLNamedIndividual> featureIndividuals = reasoner.getInstances(feature, false);
            HashSet<OWLIndividual> individualSet = new HashSet<>(featureIndividuals.getFlattened());

            synchronized (conceptAssertions) {
                conceptAssertions.put(feature, individualSet);
                currFeatureIndividuals = individualSet;
            }
        }

        if (currFeatureIndividuals.contains(individual))
            return 1;

        OWLClassExpression notClass = feature.getObjectComplementOf();

        HashSet<OWLIndividual> currNotFeatureIndividuals = conceptAssertions.get(notClass);
        if (currNotFeatureIndividuals == null) {
            NodeSet<OWLNamedIndividual> featureIndividuals = reasoner.getInstances(notClass, false);
            HashSet<OWLIndividual> individualSet = new HashSet<>(featureIndividuals.getFlattened());

            synchronized (conceptAssertions) {
                conceptAssertions.put(notClass, individualSet);
                currNotFeatureIndividuals = individualSet;
            }
        }

        return currNotFeatureIndividuals.contains(individual) ? 0 : UNCERTAIN_VALUE;
    }


}
