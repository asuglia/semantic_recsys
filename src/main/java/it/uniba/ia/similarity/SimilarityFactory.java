package it.uniba.ia.similarity;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import it.uniba.ia.data.Context;
import it.uniba.ia.data.TransferData;

/**
 * Classe factory per la misura di similarità da impiegare
 */
public class SimilarityFactory {
    /**
     * Costruisce la misura di similarità sulla base dei parametri specificati in input
     * <p>
     * Parametri da usare per la misura Similarity in context:
     * SIM_FUNCTION - identificativo misura di similarità
     * CONCEPT_SCORES_FILE - path del file contenente i pesi da associare alle feature del contesto
     * CONTEXT - istanza del contesto corrente
     * REASONER - istanza del reasoner Pellet
     *
     * @param data struttura dati contenente i parametri
     * @return funzione di similarità da utilizzare
     */
    public static Similarity create(TransferData data) {
        SimilarityFunctions id = (SimilarityFunctions) data.get("SIM_FUNCTION");
        switch(id) {
            case SIMILARITY_CONTEXT:

                String conceptScoresFile = (String) data.get("CONCEPT_SCORES_FILE");

                if (conceptScoresFile != null)
                    return new SimilarityInContext(
                        (Context) data.get("CONTEXT"),
                        (PelletReasoner) data.get("REASONER"),
                            conceptScoresFile);

                return new SimilarityInContext(
                        (Context) data.get("CONTEXT"),
                        (PelletReasoner) data.get("REASONER"));
            default:
                return null;
        }
    }
}
