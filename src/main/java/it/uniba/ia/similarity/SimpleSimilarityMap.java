package it.uniba.ia.similarity;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by asuglia on 6/7/15.
 */
public class SimpleSimilarityMap<K extends Comparable<K>, S extends Comparable<S>> implements Serializable, SimilarityMap<K, S> {
    private static final long serialVersionUID = 1484843414552451114L;
    private Map<K, Map<K, S>> simMap;
    private boolean simmetricMatrix;

    public SimpleSimilarityMap() {
        this.simMap = new HashMap<>();
    }

    public SimpleSimilarityMap(boolean simmetric) {
        this.simmetricMatrix = simmetric;
        this.simMap = new HashMap<>();
    }

    public void put(K a, K b, S score) {
        if (simmetricMatrix) {
            Map<K, S> scoresA = simMap.get(a),
                    scoresB = simMap.get(b);
            // The current similarity value is not present
            if (get(a, b) == null) {

                if (scoresA == null) {
                    scoresA = new HashMap<>();
                    simMap.put(a, scoresA);
                }

                if (scoresB == null) {
                    scoresB = new HashMap<>();
                    simMap.put(b, scoresB);
                }

            }

            scoresA.put(b, score);
            scoresB.put(a, score);

        } else {
            Map<K, S> scores = simMap.get(a);
            if (scores == null) {
                scores = new HashMap<>();
                simMap.put(a, scores);
            }

            scores.put(b, score);
        }

    }

    @Override
    public void putAll(K a, Map<K, S> scores) {
        scores.keySet().stream().forEach(b -> put(a, b, scores.get(b)));
    }

    @Override
    public LinkedHashMap<K, S> scores(K a) {
        Map<K, S> map = simMap.get(a);

        if (map != null) {
            return new LinkedHashMap<>(map);
        }

        LinkedHashMap<K, S> returnMap = new LinkedHashMap<>();

        // map is null. Construct the scores map from the other similarity values
        for (K item : simMap.keySet()) {
            if (!item.equals(a)) {
                Map<K, S> map2 = simMap.get(item);
                returnMap.put(item, map2.get(a));
            }


        }
        return returnMap;

    }

    @Override
    public LinkedHashMap<K, S> scores(K a, int k) {
        Map<K, S> map = simMap.get(a);

        if (map != null) {
            return new LinkedHashMap<>(map);
        }

        LinkedHashMap<K, S> returnMap = new LinkedHashMap<>();

        // map is null. Construct the scores map from the other similarity values
        simMap.keySet().stream().
                limit(k).
                filter(item -> !item.equals(a)).
                forEach(item -> {
                    Map<K, S> map2 = simMap.get(item);
                    returnMap.put(item, map2.get(a));
                });
        return returnMap;

    }

    @Override
    public void load(String inputFile) throws SimilarityMapException {
        try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(inputFile))) {
            this.simMap = (Map<K, Map<K, S>>) stream.readObject();
            this.simmetricMatrix = stream.readBoolean();

        } catch (ClassNotFoundException | IOException e) {
            throw new SimilarityMapException(e.getLocalizedMessage());
        }
    }

    @Override
    public void serialize(String outputFile) throws SimilarityMapException {
        try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(outputFile))) {
            stream.writeObject(simMap);
            stream.writeBoolean(simmetricMatrix);

        } catch (IOException e) {
            throw new SimilarityMapException(e.getLocalizedMessage());
        }
    }

    public S get(K a, K b) {
        Map<K, S> firstMap = simMap.get(a);
        if (firstMap != null) {
            S first = firstMap.get(b);

            if (first != null)
                return first;
        }

        Map<K, S> secMap = simMap.get(b);
        if (secMap != null) {
            S sec = secMap.get(b);

            if (sec != null)
                return sec;
        }

        return null;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(this.simMap);
        out.writeBoolean(this.simmetricMatrix);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.simMap = (Map<K, Map<K, S>>) in.readObject();
        this.simmetricMatrix = in.readBoolean();
    }

    public void clear() {
        simMap.clear();
    }

}
