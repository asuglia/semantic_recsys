package it.uniba.ia.similarity;

import weka.core.Instance;
import weka.core.NormalizableDistance;

/**
 * Created by asuglia on 7/8/15.
 */
public class WekaSimContext extends NormalizableDistance {

    private final SimilarityMap<String, Double> simMap;

    public WekaSimContext(SimilarityEngine engine) {
        this.simMap = engine.getSimilarityMap();
    }

    @Override
    public double distance(Instance a, Instance b) {
        String aId = a.stringValue(0), bId = b.stringValue(0);
        return simMap.get(aId, bId);
    }

    @Override
    public String globalInfo() {
        return "Similarity in Context function - Fanizzi et al.";
    }

    @Override
    protected double updateDistance(double currDist, double diff) {
        double result;

        result = currDist;
        result += diff * diff;

        return result;
    }

    @Override
    public String getRevision() {
        return "beta";
    }
}
