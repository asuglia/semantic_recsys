package it.uniba.ia.similarity;

/**
 * Created by asuglia on 6/27/15.
 */
public class SimilarityMapException extends Exception {
    private String errorMsg;

    public SimilarityMapException(String msg) {
        this.errorMsg = msg;
    }

    @Override
    public String getLocalizedMessage() {
        return errorMsg;
    }

    @Override
    public String getMessage() {
        return errorMsg;
    }

    @Override
    public String toString() {
        return "Similarity Map error: " + errorMsg;
    }


}
