package it.uniba.ia;

import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import it.uniba.ia.data.ItemScore;
import it.uniba.ia.data.TransferData;
import it.uniba.ia.recsys.ItemRanker;
import it.uniba.ia.recsys.RecommenderFactory;
import it.uniba.ia.similarity.SimilarityEngine;
import it.uniba.ia.util.ESWCDataManager;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Entry point per l'esecuzione dell'algoritmo K-Medoids PAM
 */
public class MainRecommenderPAM {
    private static final Logger logger = Logger.getLogger(MainRecommenderPAM.class.getName());

    /**
     * Genera le raccomandazioni usando l'algoritmo K-Medoids PAM impiegando come
     * parametri quelli specificati nel file di Properties specificato
     *
     * @param args path del file di properties - vedi rec_pam.prop
     */
    public static void main(String[] args) {
        if (args.length == 1) {
            Properties prop = new Properties();
            try {
                prop.load(new FileReader(args[0]));
                String trainFormat = prop.getProperty("train_format"),
                        testFormat = prop.getProperty("test_format"),
                        //itemInfoFile = prop.getProperty("item_file_info"),
                        //entityName = prop.getProperty("entity_name"),
                        simScoresFile = prop.getProperty("sim_scores"),
                        outputFormat = prop.getProperty("output_format"),
                        kString = prop.getProperty("k");
                String[] kStringValues = kString.split(",");

                int[] kLevels = new int[kStringValues.length];

                for (int i = 0; i < kStringValues.length; i++) {
                    kLevels[i] = Integer.parseInt(kStringValues[i]);
                }

                int folds = Integer.parseInt(prop.getProperty("folds")),
                        maxIter = Integer.parseInt(prop.getProperty("max_iter")),
                        topN = Integer.parseInt(prop.getProperty("top_n"));
                SimilarityEngine engine = new SimilarityEngine();
                engine.load(simScoresFile);

                // for each values of K
                for (int k : kLevels) {
                    // For each fold
                    for (int i = 1; i <= folds; i++) {
                        Multimap<String, String> trainRatings = ESWCDataManager.readRatings(String.format(trainFormat, i), '\t'),
                                testRatings = ESWCDataManager.readRatings(String.format(testFormat, i), '\t');

                        logger.info("Fold " + i);


                        ItemRanker ranker = RecommenderFactory.create(new TransferData(
                                new String[]{
                                        "RECTYPE",
                                        "K",
                                        "SIM_ENGINE",
                                        "MAX_ITER"
                                },
                                new Object[]{
                                        RecommenderFactory.RecType.PAM,
                                        k,
                                        engine,
                                        maxIter
                                }
                        ));


                        ranker.build(trainRatings);

                        logger.info("Recommender build for fold " + i);

                        Map<String, Set<ItemScore<String, Double>>> userRec = generateRecommendations(ranker, testRatings.keySet());
                        serializeRecommendations(String.format(outputFormat, k, i), userRec, topN);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("Incorrect number of parameters: #program_name <properties_file>");
        }


    }

    private static Map<String, Set<ItemScore<String, Double>>> generateRecommendations(ItemRanker ranker, Set<String> users) throws Exception {
        Map<String, Set<ItemScore<String, Double>>> recommendations = new HashMap<>();

        for (String user : users) {
            recommendations.put(user, ranker.rank(user));
            logger.info("Generated recommendations for user " + user);
        }

        return recommendations;
    }

    private static void serializeRecommendations(String outputFile,
                                                 Map<String, Set<ItemScore<String, Double>>> recommendations,
                                                 int topN) throws IOException {
        try (CSVPrinter printer = new CSVPrinter(new FileWriter(outputFile), CSVFormat.TDF)) {
            for (String user : recommendations.keySet()) {
                Set<ItemScore<String, Double>> recList = recommendations.get(user);

                for (ItemScore<String, Double> item : Iterables.limit(recList, topN)) {
                    printer.printRecord(user, item.getItem(), 1d);
                }
            }
        }

    }


}
