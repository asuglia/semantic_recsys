# Artificial intelligence project: Semantic RecSys #

### Summary ###

Developed during the *Artificial Intelligence* course at the University of Bari, Computer Science Department. 

The code is used in an experimental study in which have been developed two different kind of algorithm to complete the *top-n recommendation task*. Both of them are based on a similarity measure called Similarity in Context [1] which uses information taken from DBpedia. 

Two different kind of implementation have been used in order to infer user's preferences:
1. K-Medoids PAM: state-of-the-art clustering algorithm
2. NaiveKNN: naive strategy developed using only the similarity matrix

### How do I get set up? ###

It's a simply Maven project so you can set it up exactly as all the other projects.

### Contact ###
Alessandro Suglia - alessandro.suglia@yahoo.com